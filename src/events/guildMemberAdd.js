const ModerationService = require("../services/ModerationService.js");
const Constants = require("../utility/Constants.js");
const db = require("../database");
const client = require("../singletons/client.js");

client.on("guildMemberAdd", async (member) => {
  const dbUser = await db.userRepo.getUser(member.id, member.guild.id);
  const dbGuild = await db.guildRepo.getGuild(member.guild.id);

  if (dbUser.leftRoles) {
    for (let i = 0; i < dbUser.leftRoles.length; i++) {
      const role = dbUser.leftRoles[i];
      if (role !== dbGuild.roles.muted) {
        if (member.guild.roles.cache.has(role)) {
          await member.roles.add(role);
        }
      }
    }
  }

  if (
    dbGuild.roles.muted != null &&
    (await db.muteRepo.anyMute(member.id, member.guild.id))
  ) {
    const role = await member.guild.roles.fetch(dbGuild.roles.muted);

    if (
      role != null &&
      member.guild.me.permissions.has("MANAGE_ROLES") &&
      member.guild.me.roles.highest.position > role.position
    ) {
      return member.roles.add(role);
    }
    const channel = await member.guild.channels.resolve(Constants.modLog);
    const marshalRole = await member.guild.roles.fetch("293845938764644352");
    await ModerationService.tryLogAnything(
      dbGuild,
      member.guild,
      null,
      member.user.tag +
        " is muted and rejoined. I could not give them the Muted role, please do it manually.",
      null
    );
    return channel.send(marshalRole.toString());
  }
});
