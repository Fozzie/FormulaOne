const db = require("../database/index");
const Constants = require("../utility/Constants.js");
const client = require("../singletons/client.js");
const Embed = require("../structures/Embed.js");
const Punishment = require("../utility/Punishment.js");
const Discord = require("discord.js");
const StringUtil = require("../utility/StringUtil.js");
const NumberUtil = require("../utility/NumberUtil.js");
const MenuUtil = require("../utility/MenuUtil.js");
const ModerationService = require("../services/ModerationService.js");
const Sender = require("../utility/Sender.js");
const Try = require("../utility/Try.js");

const reportAmounts = {};
const reports = [];

client.on("interactionCreate", async (interaction) => {
  if (interaction.isButton()) {
    if (
      interaction.channel.id === "913752470293991424" ||
      interaction.channel.id === "913753870432370708"
    ) {
      // XP and roles channels
      if (
        (interaction.customId === "f1technical" ||
          interaction.customId === "f1serious") &&
        !interaction.member.roles.cache.has(Constants.roles.f4)
      ) {
        return interaction.reply({
          embeds: [
            new Embed({
              description: "You must have F4 role to gain access to this channel.",
              color: Constants.errorColor,
            }),
          ],
          ephemeral: true,
        });
      }

      const role = interaction.guild.roles.cache.get(
        Constants.buttonRoles[interaction.customId]
      );

      const add = !interaction.member.roles.cache.has(role.id);
      add
        ? await interaction.member.roles.add(role)
        : await interaction.member.roles.remove(role);
      interaction.reply({
        embeds: [
          new Embed({
            description: `Successfully ${add ? "added" : "removed"} the ${role} role.`,
          }),
        ],
        ephemeral: true,
      });
    } else if (
      interaction.customId.startsWith("ppage") ||
      interaction.customId.startsWith("npage")
    ) {
      const dbGuild = await db.guildRepo.getGuild(interaction.guild.id);
      if (ModerationService.getPermLevel(dbGuild, interaction.member) > 0) {
        let currentPage,
          pageOutput,
          user,
          dbUser,
          fieldsAndValues,
          embedColour,
          buttons;

        if (interaction.customId.startsWith("ppage")) {
          const split = interaction.customId.split("ppage-")[1];
          currentPage = parseInt(split.split("-")[0]);
          pageOutput = currentPage;
          const userId = split.split("-")[1];
          user = await client.users.fetch(userId);
          dbUser = await db.userRepo.getUser(user.id, interaction.guild.id);

          embedColour = split.split("-")[2];
          buttons = [
            [
              new Discord.MessageButton()
                .setCustomId(
                  "ppage-" + (currentPage - 1) + "-" + userId + "-" + embedColour
                )
                .setEmoji("⬅")
                .setStyle("SECONDARY")
                .setDisabled(currentPage - 1 === 0),
              new Discord.MessageButton()
                .setCustomId(
                  "npage-" + (currentPage - 1) + "-" + userId + "-" + embedColour
                )
                .setEmoji("➡")
                .setStyle("SECONDARY")
                .setDisabled(false),
            ],
          ];

          fieldsAndValues = await Punishment.getHistory(
            user,
            interaction.guild,
            (currentPage - 1) * 5
          );
        }

        if (interaction.customId.startsWith("npage")) {
          const split = interaction.customId.split("npage-")[1];
          currentPage = parseInt(split.split("-")[0]);
          pageOutput = currentPage + 2;
          const userId = split.split("-")[1];
          user = await client.users.fetch(userId);
          dbUser = await db.userRepo.getUser(user.id, interaction.guild.id);

          embedColour = split.split("-")[2];
          buttons = [
            [
              new Discord.MessageButton()
                .setCustomId(
                  "ppage-" + (currentPage + 1) + "-" + userId + "-" + embedColour
                )
                .setEmoji("⬅")
                .setStyle("SECONDARY")
                .setDisabled(false),
              new Discord.MessageButton()
                .setCustomId(
                  "npage-" + (currentPage + 1) + "-" + userId + "-" + embedColour
                )
                .setEmoji("➡")
                .setStyle("SECONDARY")
                .setDisabled((currentPage + 2) * 5 > dbUser.punishments.length),
            ],
          ];

          fieldsAndValues = await Punishment.getHistory(
            user,
            interaction.guild,
            (currentPage + 1) * 5
          );
        }

        const options = {
          title:
            user.tag +
            "'s Punishment History (" +
            pageOutput +
            "/" +
            (dbUser.punishments.length < 5
              ? 1
              : dbUser.punishments.length % 5 === 0
              ? dbUser.punishments.length / 5
              : Math.trunc(dbUser.punishments.length / 5) + 1) +
            ")",
          fields: [],
          color: Constants.defaultColors[embedColour],
        };
        options.footer = {
          text:
            user.tag +
            " has " +
            dbUser.currentPunishment +
            " punishments in the last 30 days.",
        };

        for (let i = 0; i < fieldsAndValues.length - 1; i++) {
          if (NumberUtil.isEven(i)) {
            options.fields.push({
              name: fieldsAndValues[i],
              value: fieldsAndValues[i + 1],
            });
          }
        }

        return interaction.update({
          embeds: [new Embed(options)],
          components: buttons.map((b) => ({ type: 1, components: b })),
        });
      }
    } else if (interaction.customId.startsWith("suggestscore")) {
      const dbGuild = await db.guildRepo.getGuild(interaction.guild.id);

      if (ModerationService.getPermLevel(dbGuild) !== 3) {
        return interaction.update({
          embeds: [
            new Embed({
              description: "Not implemented yet.",
              color: Constants.errorColor,
            }),
          ],
        });
      }
    } else if (interaction.customId.startsWith("archivelog")) {
      const split = interaction.customId.split("-");
      const userId = split[1];
      const messageId = split[2];
      const operation = split[3];

      const channel = interaction.guild.channels.cache.get(Constants.botQueueLog);
      const message = await interaction.channel.messages.fetch(messageId);
      const messageEmbed = message.embeds[0];
      const buttons = [
        [
          new Discord.MessageButton()
            .setCustomId("userid-" + userId)
            .setLabel("User ID")
            .setStyle("SECONDARY"),
        ],
      ];
      const archiveThread = channel.threads.cache.get(Constants.botQueueArchiveThread);
      if (operation === "alreadybanned") {
        const messageSent = await archiveThread.send({
          content: `${
            interaction.channel.id === Constants.stewardsQueueLog
              ? "Escalation result - "
              : ""
          }Already banned`,
          embeds: [messageEmbed],
          components: buttons.map((b) => ({ type: 1, components: b })),
        });
        if (messageSent) {
          await message.delete();
        }

        await interaction.update({
          embeds: [
            new Embed({
              description: `Successfully archived log.`,
              color: Constants.unmuteColor,
            }),
          ],
          components: [],
          ephemeral: true,
        });
      }
    } else if (
      interaction.customId.startsWith("archive") ||
      interaction.customId.startsWith("ban") ||
      interaction.customId.startsWith("unmute")
    ) {
      const dbGuild = await db.guildRepo.getGuild(interaction.guild.id);
      if (ModerationService.getPermLevel(dbGuild, interaction.member) > 0) {
        const split = interaction.customId.split("-");
        const operation = split[0];
        const userId = split[1];
        const user = await interaction.client.users.fetch(userId);
        const action = split[2];
        if (action === "cancel") {
          return interaction.update({
            embeds: [
              new Embed({
                description: `${StringUtil.upperFirstChar(operation)} cancelled.`,
                color: Constants.errorColor,
              }),
            ],
            components: [],
            ephemeral: true,
          });
        }
        if (action === "unconfirmed") {
          if (operation === "ban") {
            const guildBan = await Try(interaction.guild.bans.fetch(userId));
            if (guildBan) {
              const buttons = [
                [
                  new Discord.MessageButton()
                    .setCustomId(
                      "archivelog-" +
                        userId +
                        "-" +
                        interaction.message.id +
                        "-alreadybanned"
                    )
                    .setLabel("Yes")
                    .setStyle("SUCCESS"),
                  new Discord.MessageButton()
                    .setCustomId("archive-" + userId + "-cancel")
                    .setLabel("No")
                    .setStyle("DANGER"),
                ],
              ];

              return interaction.reply({
                embeds: [
                  new Embed({
                    description:
                      "User is already banned. Would you like to archive this?",
                    color: Constants.errorColor,
                  }),
                ],
                components: buttons.map((b) => ({ type: 1, components: b })),
                ephemeral: true,
              });
            }
          }
          if (operation === "unmute") {
            const member = interaction.guild.members.cache.get(userId);
            if (member == null) {
              return interaction.reply({
                embeds: [
                  new Embed({
                    description: "Member not found.",
                    color: Constants.errorColor,
                  }),
                ],
                ephemeral: true,
              });
            }
            if (!member.roles.cache.has(dbGuild.roles.muted)) {
              return interaction.reply({
                embeds: [
                  new Embed({
                    description: "Member is not muted.",
                    color: Constants.errorColor,
                  }),
                ],
                ephemeral: true,
              });
            }
          }
          const buttons = [
            [
              new Discord.MessageButton()
                .setCustomId(
                  operation + "-" + userId + "-confirmed-" + interaction.message.id
                )
                .setLabel("Yes")
                .setStyle("SUCCESS"),
              new Discord.MessageButton()
                .setCustomId(operation + "-" + userId + "-cancel")
                .setLabel("No")
                .setStyle("DANGER"),
            ],
          ];
          return interaction.reply({
            embeds: [
              new Embed({
                description: `Are you sure you want to ${operation} ${StringUtil.boldify(
                  user.tag
                )}?`,
                color: operation === "ban" ? Constants.banColor : Constants.unmuteColor,
              }),
            ],
            components: buttons.map((b) => ({ type: 1, components: b })),
            ephemeral: true,
          });
        }
        if (action.startsWith("confirmed")) {
          const botQueueLog = interaction.guild.channels.cache.get(
            Constants.botQueueLog
          );
          const message = await interaction.channel.messages.fetch(split[3]);
          const messageEmbed = message.embeds[0];
          const buttons = [
            [
              new Discord.MessageButton()
                .setCustomId("userid-" + userId)
                .setLabel("User ID")
                .setStyle("SECONDARY"),
            ],
          ];
          const archiveThread = botQueueLog.threads.cache.get(
            Constants.botQueueArchiveThread
          );

          if (operation === "ban") {
            await interaction.guild.members.ban(user, {
              days: 1,
              reason: `(${interaction.user.tag}) violation ban.`,
            });
            await db.userRepo.upsertUser(
              userId,
              interaction.guild.id,
              new db.updates.Push("punishments", {
                date: Date.now(),
                escalation: "Ban",
                reason: "Violation ban",
                mod: interaction.user.tag,
                channelId: Constants.botQueueLog,
              })
            );
            await interaction.update({
              embeds: [
                new Embed({
                  description: `Successfully banned ${StringUtil.boldify(user.tag)}.`,
                  color: Constants.unmuteColor,
                }),
              ],
              components: [],
              ephemeral: true,
            });
            const messageSent = await archiveThread.send({
              content: `${
                interaction.channel.id === Constants.stewardsQueueLog
                  ? "Escalation result - "
                  : ""
              }Banned by ${StringUtil.boldify(interaction.user.tag)}`,
              embeds: [messageEmbed],
              components: buttons.map((b) => ({ type: 1, components: b })),
            });
            if (messageSent) {
              await message.delete();
            }
            return ModerationService.tryModLog(
              dbGuild,
              interaction.guild,
              "Ban",
              Constants.banColor,
              "moderation-queue violation ban",
              interaction.user,
              user
            );
          }
          if (operation === "unmute") {
            const member = interaction.guild.members.cache.get(userId);
            if (member == null) {
              return interaction.update({
                embeds: [
                  new Embed({
                    description: "Member not found.",
                    color: Constants.errorColor,
                  }),
                ],
                components: [],
                ephemeral: true,
              });
            }
            if (!member.roles.cache.has(dbGuild.roles.muted)) {
              return interaction.update({
                embeds: [
                  new Embed({
                    description: "Member is not muted.",
                    color: Constants.errorColor,
                  }),
                ],
                components: [],
                ephemeral: true,
              });
            }
            const role = await interaction.guild.roles.fetch(dbGuild.roles.muted);
            await member.roles.remove(
              role,
              `${interaction.user.tag} violation unmute.`
            );
            await interaction.update({
              embeds: [
                new Embed({
                  description: `Successfully unmuted ${StringUtil.boldify(
                    member.user.tag
                  )}.`,
                  color: Constants.unmuteColor,
                }),
              ],
              components: [],
              ephemeral: true,
            });
            const messageSent = await archiveThread.send({
              content: `${
                interaction.channel.id === Constants.stewardsQueueLog
                  ? "Escalation result - "
                  : ""
              }Unmuted by ${StringUtil.boldify(interaction.user.tag)}`,
              embeds: [messageEmbed],
              components: buttons.map((b) => ({ type: 1, components: b })),
            });
            if (messageSent) {
              await message.delete();
            }
            return ModerationService.tryModLog(
              dbGuild,
              interaction.guild,
              "Unmute",
              Constants.unmuteColor,
              "moderation-queue violation unmute",
              interaction.user,
              member.user
            );
          }
        }
      } else {
        return interaction.reply({
          embeds: [
            new Embed({
              description: `You must be a Marshal in order to use this command.`,
              color: Constants.errorColor,
            }),
          ],
          ephemeral: true,
        });
      }
    } else if (interaction.customId.startsWith("userid")) {
      const userId = interaction.customId.split("userid-")[1];
      return interaction.reply({ content: userId, ephemeral: true });
    } else if (interaction.customId.startsWith("ignore")) {
      const split = interaction.customId.split("-");
      const userId = split[1];
      const botQueueLog = interaction.guild.channels.cache.get(Constants.botQueueLog);
      const message = await interaction.channel.messages.fetch(interaction.message.id);

      if (message != null) {
        const messageEmbed = message.embeds[0];
        const archiveThread = botQueueLog.threads.cache.get(
          Constants.botQueueArchiveThread
        );

        const buttons = [
          [
            new Discord.MessageButton()
              .setCustomId("userid-" + userId)
              .setLabel("User ID")
              .setStyle("SECONDARY"),
          ],
        ];
        const messageSent = await archiveThread.send({
          content: `${
            interaction.channel.id === Constants.stewardsQueueLog
              ? "Escalation result - "
              : ""
          }Ignored by ${StringUtil.boldify(interaction.user.tag)}`,
          embeds: [messageEmbed],
          components: buttons.map((b) => ({ type: 1, components: b })),
        });
        if (messageSent) {
          await message.delete();
          return interaction.reply({
            embeds: [
              new Embed({
                description: "Successfully ignored log.",
                color: Constants.unmuteColor,
              }),
            ],
            ephemeral: true,
          });
        }
        return interaction.reply({
          embeds: [
            new Embed({
              description: "Error ignoring log.",
              color: Constants.errorColor,
            }),
          ],
          ephemeral: true,
        });
      }
    } else if (interaction.customId.startsWith("punish")) {
      const split = interaction.customId.split("-");
      const targetMember = interaction.guild.members.cache.get(split[1]);
      if (targetMember == null) {
        return interaction.reply({
          embeds: [
            new Embed({
              description: "Member not found.",
              color: Constants.errorColor,
            }),
          ],
          ephemeral: true,
        });
      }

      const dbUser = await db.userRepo.getUser(targetMember.id, interaction.guild.id);

      if (dbUser.currentPunishment > 4) {
        return interaction.reply({
          embeds: [
            new Embed({
              description: `${StringUtil.boldify(
                interaction.member.user.tag
              )}, ${StringUtil.boldify(
                targetMember.user.tag
              )} has exceeded 5 punishments in the last 30 days, escalate their punishment manually.`,
              color: Constants.errorColor,
            }),
          ],
          ephemeral: true,
        });
      }

      const selectmenu = [
        [
          new Discord.MessageSelectMenu()
            .setCustomId(
              "amountselect-PunishQ-" +
                targetMember.id +
                "-" +
                split[2] +
                "-" +
                split[3] +
                "-" +
                interaction.message.id
            )
            .setPlaceholder("Select amount")
            .addOptions(MenuUtil.getAmountMap(dbUser)),
        ],
      ];
      return interaction.reply({
        content: "Please select a punishment amount.",
        components: selectmenu.map((b) => ({ type: 1, components: b })),
        ephemeral: true,
      });
    } else if (interaction.customId.startsWith("escalate")) {
      const split = interaction.customId.split("-");
      const targetUserId = split[1];
      const targetChannelId = split[2];
      const targetMessageId = split[3];

      const botQueueLog = interaction.guild.channels.cache.get(Constants.botQueueLog);
      const stewardsQueue = interaction.guild.channels.cache.get(
        Constants.stewardsQueueLog
      );
      const archiveThread = botQueueLog.threads.cache.get(
        Constants.botQueueArchiveThread
      );

      const message = await interaction.channel.messages.fetch(interaction.message.id);
      const messageEmbed = message.embeds[0];

      const buttons = [
        [
          new Discord.MessageButton()
            .setCustomId("userid-" + targetUserId)
            .setLabel("User ID")
            .setStyle("SECONDARY"),
          new Discord.MessageButton()
            .setCustomId(
              "punish-" + targetUserId + "-" + targetChannelId + "-" + targetMessageId
            )
            .setLabel("Punish")
            .setStyle("DANGER"),
          new Discord.MessageButton()
            .setCustomId("ban-" + targetUserId + "-unconfirmed")
            .setLabel("Ban")
            .setStyle("DANGER"),
          new Discord.MessageButton()
            .setCustomId("ignore-" + targetUserId)
            .setLabel("Ignore")
            .setStyle("SECONDARY"),
        ],
      ];

      const archiveButtons = [
        [
          new Discord.MessageButton()
            .setCustomId("userid-" + targetUserId)
            .setLabel("User ID")
            .setStyle("SECONDARY"),
        ],
      ];

      const messageSent = await stewardsQueue.send({
        content: `<@&177408501268611073>, Escalated by ${StringUtil.boldify(
          interaction.user.tag
        )}`,
        embeds: [messageEmbed],
        components: buttons.map((b) => ({ type: 1, components: b })),
      });
      if (messageSent) {
        const archiveSent = await archiveThread.send({
          content: `Escalated by ${StringUtil.boldify(interaction.user.tag)}`,
          embeds: [messageEmbed],
          components: archiveButtons.map((b) => ({ type: 1, components: b })),
        });
        if (archiveSent) {
          await message.delete();
          return interaction.reply({
            embeds: [
              new Embed({
                description: "Successfully escalated log.",
                color: Constants.unmuteColor,
              }),
            ],
            ephemeral: true,
          });
        }
      }
      return interaction.reply({
        embeds: [
          new Embed({
            description: "Error escalating log.",
            color: Constants.errorColor,
          }),
        ],
        ephemeral: true,
      });
    } else if (interaction.customId.startsWith("publish")) {
      const split = interaction.customId.split("-");
      const poster = split[1];

      if (!interaction.message.crosspostable) {
        await interaction.reply({
          embeds: [
            new Embed({
              description: `${StringUtil.boldify(
                interaction.member.user.tag
              )}, Cannot publish message, maybe it is already published?`,
              color: Constants.errorColor,
            }),
          ],
          ephemeral: true,
        });
        return interaction.message.edit({
          content: interaction.message.content,
          components: [],
        });
      }

      const dbGuild = await db.guildRepo.getGuild(interaction.guild.id);
      if (ModerationService.getPermLevel(dbGuild, interaction.member) === 0) {
        if (!interaction.member.roles.cache.has(Constants.roles.f1)) {
          return interaction.reply({
            embeds: [
              new Embed({
                description: `${StringUtil.boldify(
                  interaction.member.user.tag
                )}, You must have the F1 role in order to use this command.`,
                color: Constants.errorColor,
              }),
            ],
            ephemeral: true,
          });
        }

        if (interaction.user.id !== poster) {
          return interaction.reply({
            embeds: [
              new Embed({
                description: `${StringUtil.boldify(
                  interaction.member.user.tag
                )}, Only the original poster can publish a message.`,
                color: Constants.errorColor,
              }),
            ],
            ephemeral: true,
          });
        }
      }

      await interaction.message.crosspost();
      await interaction.message.edit({
        content: interaction.message.content,
        components: [],
      });
      return interaction.reply({
        embeds: [
          new Embed({
            description: "Successfully published message.",
            color: Constants.unmuteColor,
          }),
        ],
        ephemeral: true,
      });
    }
  } else if (interaction.isContextMenu()) {
    if (interaction.commandName === "Punish" || interaction.commandName === "Report") {
      const command = interaction.commandName;
      const dbGuild = await db.guildRepo.getGuild(interaction.guild.id);
      const member = interaction.member;

      if (command === "Punish") {
        if (ModerationService.getPermLevel(dbGuild, member) === 0) {
          return interaction.reply({
            embeds: [
              new Embed({
                description: `${StringUtil.boldify(
                  member.user.tag
                )}, You must be a Marshal in order to use this command.`,
                color: Constants.errorColor,
              }),
            ],
            ephemeral: true,
          });
        }
      }
      if (command === "Report") {
        const userReports = reportAmounts[member.id];
        if (userReports) {
          if (userReports.reportStart + 60000 > Date.now()) {
            reportAmounts[member.id].reportCount++;
            if (userReports.reportCount === 5) {
              return interaction.reply({
                embeds: [
                  new Embed({
                    description: `${StringUtil.boldify(
                      member.user.tag
                    )}, You are being rate limited.`,
                    color: Constants.errorColor,
                  }),
                ],
                ephemeral: true,
              });
            }
          } else {
            reportAmounts[member.id] = {
              reportStart: Date.now(),
              reportCount: 0,
            };
          }
        } else {
          reportAmounts[member.id] = {
            reportStart: Date.now(),
            reportCount: 0,
          };
        }
      }

      const message = interaction.options.getMessage("message");
      const targetMember = interaction.guild.members.cache.get(message.author.id);
      if (targetMember == null) {
        return interaction.reply({
          embeds: [
            new Embed({
              description: `${StringUtil.boldify(member.user.tag)}, Member not found.`,
              color: Constants.errorColor,
            }),
          ],
          ephemeral: true,
        });
      }

      if (ModerationService.getPermLevel(dbGuild, targetMember) > 0) {
        return interaction.reply({
          embeds: [
            new Embed({
              description: `${StringUtil.boldify(
                member.user.tag
              )}, You may not use this command on a moderator.`,
              color: Constants.errorColor,
            }),
          ],
          ephemeral: true,
        });
      }

      const dbUser = await db.userRepo.getUser(targetMember.id, interaction.guild.id);

      if (dbUser.currentPunishment > 4) {
        return interaction.reply({
          embeds: [
            new Embed({
              description: `${StringUtil.boldify(
                member.user.tag
              )}, ${StringUtil.boldify(
                targetMember.user.tag
              )} has exceeded 5 punishments in the last 30 days, escalate their punishment manually.`,
              color: Constants.errorColor,
            }),
          ],
          ephemeral: true,
        });
      }

      if (command === "Punish") {
        const selectmenu = [
          [
            new Discord.MessageSelectMenu()
              .setCustomId(
                "amountselect-" +
                  command +
                  "-" +
                  targetMember.id +
                  "-" +
                  interaction.channel.id +
                  "-" +
                  message.id
              )
              .setPlaceholder("Select amount")
              .addOptions(MenuUtil.getAmountMap(dbUser)),
          ],
        ];
        return interaction.reply({
          content: "Please select a punishment amount.",
          components: selectmenu.map((b) => ({ type: 1, components: b })),
          ephemeral: true,
        });
      } else if (command === "Report") {
        // const selectmenu = [
        //   [
        //     new Discord.MessageSelectMenu()
        //       .setCustomId(
        //         "ruleselect-" +
        //           command +
        //           "-" +
        //           targetMember.id +
        //           "-" +
        //           interaction.channel.id +
        //           "-" +
        //           message.id
        //       )
        //       .setPlaceholder("Select rule")
        //       .addOptions(MenuUtil.getRulesMap()),
        //   ],
        // ];
        // return interaction.reply({
        //   content: "Please select a rule.",
        //   components: selectmenu.map((b) => ({ type: 1, components: b })),
        //   ephemeral: true,
        // });
        if (
          !reports.some(
            (report) =>
              report.channelId === interaction.channel.id &&
              report.messageId === message.id
          )
        ) {
          let sendMessage =
            `Report in ${interaction.channel.toString()} [Jump to message](${
              message.url
            })\n` +
            `\n${StringUtil.boldify("Content:")} ${StringUtil.removeClickableLinks(
              StringUtil.escape(StringUtil.maxLength(message.content, true))
            )}` +
            `\n${StringUtil.boldify("Reporter:")} ${interaction.user.tag}`;
          reports.push({
            channelId: interaction.channel.id,
            messageId: message.id,
          });
          await ModerationService.tryLogBotQueue(
            Constants.botQueueLog,
            interaction.guild,
            targetMember.user,
            sendMessage,
            interaction.channel.id,
            message.id,
            Constants.banColor,
            true,
            false,
            false,
            true
          );
        }
        return interaction.reply({
          embeds: [
            new Embed({
              description: `Successfully reported ${StringUtil.boldify(
                targetMember.user.tag
              )}.`,
              color: Constants.unmuteColor,
            }),
          ],
          content: null,
          components: [],
          ephemeral: true,
        });
      }
    }
  } else if (interaction.isSelectMenu()) {
    if (interaction.customId.startsWith("amountselect")) {
      const split = interaction.customId.split("-");
      const command = split[1];
      const targetId = split[2];
      const channelId = split[3];
      const messageId = split[4];
      const interactionMessageId = split[5];
      const amount = interaction.values[0];

      const selectmenu = [
        [
          new Discord.MessageSelectMenu()
            .setCustomId(
              "ruleselect-" +
                command +
                "-" +
                targetId +
                "-" +
                channelId +
                "-" +
                messageId +
                "-" +
                interactionMessageId +
                "-" +
                amount
            )
            .setPlaceholder("Select rule")
            .addOptions(MenuUtil.getRulesMap()),
        ],
      ];
      return interaction.update({
        content: "Please select a rule.",
        components: selectmenu.map((b) => ({ type: 1, components: b })),
        ephemeral: true,
      });
    } else if (interaction.customId.startsWith("ruleselect")) {
      const split = interaction.customId.split("-");
      const action = split[1];
      const targetMember = await interaction.guild.members.cache.get(split[2]);
      const targetChannel = interaction.guild.channels.cache.get(split[3]);
      const targetMessage = await targetChannel.messages.fetch(split[4]);
      const amount = parseInt(split[6]);
      const dbGuild = await db.guildRepo.getGuild(interaction.guild.id);
      const rules = Constants.rules;
      const rule = parseInt(interaction.values[0].split("-")[1]);
      if (action.startsWith("Punish")) {
        // Create dummy msg and args objects to map to function.
        const msg = {},
          args = {};
        msg.dbGuild = dbGuild;
        msg.guild = interaction.guild;
        msg.channel = interaction.channel;
        msg.author = interaction.user;
        msg.sender = new Sender(msg);
        args.member = targetMember;
        args.reason = `Rule ${rule + 1} - ${rules[rule]}`;
        args.messageContent = StringUtil.removeClickableLinks(
          StringUtil.escape(StringUtil.maxLength(targetMessage.content, true))
        );
        args.targetChannel = targetChannel.id;
        await Punishment.runPunishCommand(msg, args, true, amount);
        if (action === "PunishQ") {
          await Try(targetMessage.delete());
        }

        if (
          interaction.channel.id === Constants.botQueueLog ||
          interaction.channel.id === Constants.stewardsQueueLog
        ) {
          const message = await interaction.channel.messages.fetch(split[5]);
          const messageEmbed = message.embeds[0];

          const botQueueLog = interaction.guild.channels.cache.get(
            Constants.botQueueLog
          );
          const archiveThread = botQueueLog.threads.cache.get(
            Constants.botQueueArchiveThread
          );

          const buttons = [
            [
              new Discord.MessageButton()
                .setCustomId("userid-" + targetMessage.id)
                .setLabel("User ID")
                .setStyle("SECONDARY"),
            ],
          ];
          const messageSent = await archiveThread.send({
            content: `${
              interaction.channel.id === Constants.stewardsQueueLog
                ? "Escalation result - "
                : ""
            }Punished by ${StringUtil.boldify(interaction.user.tag)}`,
            embeds: [messageEmbed],
            components: buttons.map((b) => ({ type: 1, components: b })),
          });
          if (messageSent) {
            await message.delete();
          }
        }
        return interaction.update({
          embeds: [
            new Embed({
              description: `Successfully punished ${StringUtil.boldify(
                targetMember.user.tag
              )}.`,
              color: Constants.unmuteColor,
            }),
          ],
          content: null,
          components: [],
          ephemeral: true,
        });
      } else if (action === "Report") {
        // if (
        //   !reports.some(
        //     (report) =>
        //       report.channelId === interaction.channel.id &&
        //       report.messageId === targetMessage.id
        //   )
        // ) {
        //   let message =
        //     `Report in ${interaction.channel.toString()} [Jump to message](${
        //       targetMessage.url
        //     })\n` +
        //     `\n${StringUtil.boldify("Content:")} ${StringUtil.removeClickableLinks(
        //       StringUtil.escape(StringUtil.maxLength(targetMessage.content, true))
        //     )}` +
        //     `\n${StringUtil.boldify("Rule:")} ${rule + 1} - ${rules[rule]}` +
        //     `\n${StringUtil.boldify("Reporter:")} ${interaction.user.tag}`;
        //   reports.push({
        //     channelId: interaction.channel.id,
        //     messageId: targetMessage.id,
        //   });
        //   await ModerationService.tryLogBotQueue(
        //     Constants.botQueueLog,
        //     interaction.guild,
        //     targetMember.user,
        //     message,
        //     interaction.channel.id,
        //     targetMessage.id,
        //     Constants.banColor,
        //     true,
        //     false,
        //     false,
        //     true
        //   );
        // }
        // return interaction.update({
        //   embeds: [
        //     new Embed({
        //       description: `Successfully reported ${StringUtil.boldify(
        //         targetMember.user.tag
        //       )}.`,
        //       color: Constants.unmuteColor,
        //     }),
        //   ],
        //   content: null,
        //   components: [],
        //   ephemeral: true,
        // });
      }
    }
  }
});
