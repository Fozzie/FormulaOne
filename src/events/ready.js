const Logger = require("../utility/Logger.js");
const client = require("../singletons/client.js");

client.on("ready", async () => {
  (async () => {
    await Logger.log("Formula One has successfully connected.", "INFO");
  })().catch((err) => Logger.handleError(err));
});
