const Logger = require("../utility/Logger.js");
const client = require("../singletons/client.js");
const Try = require("../utility/Try.js");
const db = require("../database/index.js");
const ChatService = require("../services/ChatService.js");

client.on("messageUpdate", (oldMessage, newMessage) => {
  (async () => {
    if (newMessage.author.bot || oldMessage.content === newMessage.content) {
      return;
    }

    const msg = newMessage;
    if (msg.content.length > 1) {
      const inGuild = msg.guild != null;
      if (inGuild) {
        msg.dbUser = await db.userRepo.getUser(msg.author.id, msg.guild.id);
        msg.dbGuild = await db.guildRepo.getGuild(msg.guild.id);

        if (msg.channel.id !== "314949863911587840") {
          // paddock-club
          await Try(ChatService.checkPerspective(msg));
        }
        await Try(ChatService.runChecks(msg));
      }
    }
  })().catch((err) => Logger.handleError(err));
});
