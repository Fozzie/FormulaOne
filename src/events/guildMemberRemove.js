const db = require("../database");
const client = require("../singletons/client.js");

client.on("guildMemberRemove", async (member) => {
  return db.userRepo.upsertUser(member.id, member.guild.id, {
    $set: {
      leftRoles: [...member.roles.cache.keys()].filter(
        (roleId) => roleId !== member.guild.id
      ),
    },
  }); // Map to role ID and filter out the @everyone role.
});
