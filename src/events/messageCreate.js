const Logger = require("../utility/Logger.js");
const client = require("../singletons/client.js");
const db = require("../database/index.js");
const discord = require("discord.js");
const patron = require("patron");
const Try = require("../utility/Try.js");
const Sender = require("../utility/Sender.js");
const Constants = require("../utility/Constants.js");
const NumberUtil = require("../utility/NumberUtil.js");
const StringUtil = require("../utility/StringUtil.js");
const Similarity = require("../utility/Similarity.js");
const ChatService = require("../services/ChatService.js");
const registry = require("../singletons/registry.js");
const { Handler } = require("patron");
const handler = new Handler({ registry });

client.on("messageCreate", (msg) => {
  // g
  (async () => {
    if (msg.author.bot) {
      return;
    }

    const inGuild = msg.guild != null;
    if (inGuild) {
      if (!msg.guild.available) {
        return Logger.log("Guild is currently unavailable", "ERROR");
      }
      msg.dbUser = await db.userRepo.getUser(msg.author.id, msg.guild.id);
      msg.dbGuild = await db.guildRepo.getGuild(msg.guild.id);
    }

    const originalContent = msg.content;
    msg.content = msg.content.replaceAll("\u0000", ""); // Remove \u0000 character padding.
    if (msg.content !== originalContent) {
      await Logger.log("Removed padding: " + msg.content, "INFO");
    }

    const sender = new Sender(msg);

    msg.sender = sender;

    if (inGuild) {
      if (msg.content.length > 1) {
        if (msg.channel.id !== "314949863911587840") {
          // paddock-club
          await Try(ChatService.checkPerspective(msg));
        }
        await Try(ChatService.runChecks(msg));
      }
      if (!Constants.regexes.prefix.test(msg.content)) {
        return ChatService.checkWord(msg);
      }

      if (msg.dbUser.blocklisted) {
        return Logger.log(
          `User ${msg.author.tag} is currently blocklisted from Formula One.`,
          "INFO"
        );
      }
    }

    await Logger.log(
      "Message Id: " +
        msg.id +
        " | User Id: " +
        msg.author.id +
        (inGuild === true ? " | Guild Id: " + msg.guild.id : "") +
        " | User: " +
        msg.author.tag +
        (inGuild ? " | Guild: " + msg.guild.name : "") +
        " | Content: " +
        msg.content,
      "DEBUG"
    );

    const result = await handler.run(msg, Constants.prefix);

    if (result.type !== patron.ResultType.Success) {
      let message;

      switch (result.type) {
        case patron.ResultType.Unknown: {
          if (result.name != null) {
            const similarCommand = Similarity.command(msg.client.registry, result.name);

            if (similarCommand != null) {
              await Try(
                sender.reply(
                  "Did you mean `" +
                    Constants.prefix +
                    StringUtil.upperFirstChar(similarCommand) +
                    "`?"
                )
              );
            }
          }
          return;
        }
        case patron.ResultType.Cooldown: {
          const cooldown = NumberUtil.msToTime(result.remaining);

          return Try(
            sender.send(
              "Hours: " +
                cooldown.hours +
                "\nMinutes: " +
                cooldown.minutes +
                "\nSeconds: " +
                cooldown.seconds,
              {
                title: StringUtil.upperFirstChar(result.command.names[0]) + " Cooldown",
                color: Constants.errorColor,
              }
            )
          );
        }
        case patron.ResultType.Error:
          if (result.error instanceof discord.DiscordAPIError) {
            if (
              result.error.code === 0 ||
              result.error.code === 403 ||
              result.error.code === 404 ||
              result.error.code === 50001 ||
              result.error.code === 50013
            ) {
              message = `I do not have permission to do that (${result.error.code}).`;
            } else if (result.error.code >= 500 && result.error.code < 600) {
              message = `Discord Internal Server error, check Discord\'s [status](https://status.discordapp.com/) (${result.error.code}).`;
            } else {
              message = `${result.error.message} (${result.error.code}).`;
            }
          } else {
            message = result.error.message;
            await Logger.handleError(result.error);
          }
          break;
        case patron.ResultType.ArgumentCount:
          message =
            "You must provide all required<> arguments.\n**Usage:** `" +
            Constants.prefix +
            result.command.getUsage() +
            "`\n**Example:** `" +
            Constants.prefix +
            result.command.getExample() +
            "`";
          break;
        case patron.ResultType.TypeReader:
        case patron.ResultType.Precondition:
          message = result.reason;
          break;
        default:
          message = result.reason;
          await Logger.handleError(result.reason);
          break;
      }

      await Logger.log(
        "Unsuccessful command result: " + msg.id + " | Reason: " + message,
        "DEBUG"
      );

      return Try(sender.reply(message, { color: Constants.errorColor }));
    }

    return Logger.log("Successful command result: " + msg.id, "DEBUG");
  })().catch((err) => Logger.handleError(err));
});
