const db = require("../database");
const client = require("../singletons/client.js");

client.on("guildMemberUpdate", async (oldMember, newMember) => {
  const dbGuild = await db.guildRepo.getGuild(newMember.guild.id);

  if (
    dbGuild.roles.muted != null &&
    oldMember.roles.cache.has(dbGuild.roles.muted) &&
    !newMember.roles.cache.has(dbGuild.roles.muted) &&
    (await db.muteRepo.anyMute(newMember.id, newMember.guild.id))
  ) {
    return db.muteRepo.deleteMute(newMember.id, newMember.guild.id);
  }
});
