const discord = require("discord.js");
const Constants = require("../utility/Constants.js");

module.exports = new discord.Client({
  makeCache: Constants.cacheOptions,
  intents: Constants.intents,
  presence: Constants.presence,
});
