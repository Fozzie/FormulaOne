const { Registry } = require("patron");

module.exports = new Registry({
  caseSensitive: false,
});
