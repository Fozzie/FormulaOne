const patron = require("patron");
const ModerationService = require("../../services/ModerationService.js");

class Administrator extends patron.Precondition {
  constructor() {
    super({
      name: "administrator",
    });
  }

  async run(command, msg) {
    if (ModerationService.getPermLevel(msg.dbGuild, msg.member) >= 2) {
      return patron.PreconditionResult.fromSuccess();
    }

    return patron.PreconditionResult.fromFailure(
      command,
      "You must be a Steward in order to use this command."
    );
  }
}

module.exports = new Administrator();
