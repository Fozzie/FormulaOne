const patron = require("patron");
const ModerationService = require("../../services/ModerationService.js");

class Moderator extends patron.Precondition {
  constructor() {
    super({
      name: "moderator",
    });
  }

  async run(command, msg) {
    if (ModerationService.getPermLevel(msg.dbGuild, msg.member) >= 1) {
      return patron.PreconditionResult.fromSuccess();
    }

    return patron.PreconditionResult.fromFailure(
      command,
      "You must be a Marshal in order to use this command."
    );
  }
}

module.exports = new Moderator();
