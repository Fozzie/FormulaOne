const patron = require("patron");
const ModerationService = require("../../services/ModerationService.js");

class Owner extends patron.Precondition {
  constructor() {
    super({
      name: "owner",
    });
  }

  async run(command, msg) {
    if (ModerationService.getPermLevel(msg.dbGuild, msg.member) === 3) {
      return patron.PreconditionResult.fromSuccess();
    }

    return patron.PreconditionResult.fromFailure(
      command,
      "You must be an owner in order to use this command."
    );
  }
}

module.exports = new Owner();
