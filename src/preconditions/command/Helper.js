const patron = require("patron");
const Constants = require("../../utility/Constants.js");
const ModerationService = require("../../services/ModerationService.js");

class Helper extends patron.Precondition {
  constructor() {
    super({
      name: "helper",
    });
  }

  async run(command, msg) {
    const helper = await msg.guild.roles.fetch(Constants.roles.helper);
    if (
      msg.member.roles.cache.has(helper.id) ||
      ModerationService.getPermLevel(msg.dbGuild, msg.member) > 0
    ) {
      return patron.PreconditionResult.fromSuccess();
    }

    return patron.PreconditionResult.fromFailure(
      command,
      "You must be a Helper in order to use this command."
    );
  }
}

module.exports = new Helper();
