const patron = require("patron");
const Constants = require("../../utility/Constants.js");

class Clear extends patron.Precondition {
  constructor() {
    super({
      name: "clear",
    });
  }

  async run(command, msg) {
    if (msg.channel.id === Constants.modLog) {
      await patron.PreconditionResult.fromFailure(
        command,
        "You cannot clear messages in the F1 Logs channel."
      );
    }

    return patron.PreconditionResult.fromSuccess();
  }
}

module.exports = new Clear();
