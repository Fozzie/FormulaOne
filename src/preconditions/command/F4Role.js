const patron = require("patron");
const Constants = require("../../utility/Constants.js");

class F4Role extends patron.Precondition {
  constructor() {
    super({
      name: "f4role",
    });
  }

  async run(command, msg) {
    const f4 = await msg.guild.roles.fetch(Constants.roles.f4);
    if (msg.member.roles.cache.has(f4.id)) {
      return patron.PreconditionResult.fromSuccess();
    }

    return patron.PreconditionResult.fromFailure(
      command,
      "You must have the F4 role in order to use this command."
    );
  }
}

module.exports = new F4Role();
