const patron = require("patron");

class NoSelf extends patron.ArgumentPrecondition {
  constructor() {
    super({
      name: "noself",
    });
  }

  async run(value, command, msg, argument, args) {
    if (value.id !== msg.author.id) {
      return patron.PreconditionResult.fromSuccess();
    }

    return patron.PreconditionResult.fromFailure(
      command,
      "You may not use this command on yourself."
    );
  }
}

module.exports = new NoSelf();
