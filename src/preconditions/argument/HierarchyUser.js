const patron = require("patron");

class HierarchyUser extends patron.ArgumentPrecondition {
  constructor() {
    super({
      name: "hierarchyuser",
    });
  }
  async run(value, command, msg, argument, args) {
    if (value.position < msg.member.roles.highest.position) {
      return patron.PreconditionResult.fromSuccess();
    }

    return patron.PreconditionResult.fromFailure(
      command,
      "You are not higher in role hierarchy than " + value.toString() + "."
    );
  }
}

module.exports = new HierarchyUser();
