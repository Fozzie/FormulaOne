const patron = require("patron");

class FlairLength extends patron.ArgumentPrecondition {
  constructor() {
    super({
      name: "flairlength",
    });
  }
  async run(value, command, msg, argument, args) {
    const newNickname = `${msg.author.username} [${value}]`;
    if (newNickname.length <= 32) {
      return patron.PreconditionResult.fromSuccess();
    }

    return patron.PreconditionResult.fromFailure(
      command,
      `The nickname length (username, brackets and flair) must not exceed 32 characters.`
    );
  }
}

module.exports = new FlairLength();
