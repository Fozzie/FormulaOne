const ModerationService = require("../../services/ModerationService.js");
const patron = require("patron");
const discord = require("discord.js");

class NoModerator extends patron.ArgumentPrecondition {
  constructor() {
    super({
      name: "nomoderator",
    });
  }
  async run(value, command, msg, argument, args) {
    if (value instanceof discord.User) {
      try {
        value = await msg.guild.members.fetch(value.id);
      } catch (e) {
        if (e.code === 10007) {
          // Unknown Member
          return patron.PreconditionResult.fromSuccess();
        } else {
          return patron.PreconditionResult.fromFailure(
            command,
            `Could not fetch target member (${e.code})`
          );
        }
      }
    }

    if (ModerationService.getPermLevel(msg.dbGuild, value) === 0) {
      return patron.PreconditionResult.fromSuccess();
    }

    return patron.PreconditionResult.fromFailure(
      command,
      "You may not use this command on a moderator."
    );
  }
}

module.exports = new NoModerator();
