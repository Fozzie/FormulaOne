const patron = require("patron");

class Hierarchy extends patron.ArgumentPrecondition {
  constructor() {
    super({
      name: "hierarchy",
    });
  }
  async run(value, command, msg, argument, args) {
    if (value.position < msg.guild.me.roles.highest.position) {
      return patron.PreconditionResult.fromSuccess();
    }

    return patron.PreconditionResult.fromFailure(
      command,
      msg.client.user.username +
        " must be higher in role hierarchy than " +
        value.toString() +
        "."
    );
  }
}

module.exports = new Hierarchy();
