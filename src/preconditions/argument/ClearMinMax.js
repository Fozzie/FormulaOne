const patron = require("patron");
const Constants = require("../../utility/Constants.js");

class ClearMinMax extends patron.ArgumentPrecondition {
  constructor() {
    super({
      name: "clearminmax",
    });
  }
  async run(value, command, msg, argument, args) {
    if (value <= Constants.clear.max && value >= Constants.clear.min) {
      return patron.PreconditionResult.fromSuccess();
    }

    return patron.PreconditionResult.fromFailure(
      command,
      `The amount of messages to clear must be between ${Constants.clear.min} and ${Constants.clear.max}.`
    );
  }
}

module.exports = new ClearMinMax();
