const patron = require("patron");

class StatusType extends patron.ArgumentPrecondition {
  constructor() {
    super({
      name: "statustype",
    });
  }
  async run(value, command, msg, argument, args) {
    const validTypes = ["PLAYING", "WATCHING", "LISTENING", "STREAMING", "COMPETING"];
    const type = value.toUpperCase();
    if (!validTypes.includes(type)) {
      return patron.PreconditionResult.fromFailure(
        command,
        `Invalid type, types:\n\n${validTypes.join(", ")}.`
      );
    }

    return patron.PreconditionResult.fromSuccess();
  }
}

module.exports = new StatusType();
