const Constants = require("../utility/Constants.js");
const ModerationService = require("../services/ModerationService.js");
const OneWordService = require("../services/OneWordService.js");
const StringUtil = require("../utility/StringUtil.js");
const Perspective = require("../utility/Perspective.js");
const SafeBrowsing = require("../utility/SafeBrowsing.js");
const YouTubeData = require("../utility/YouTubeData.js");
const axios = require("axios");
const SocksProxyAgent = require("socks-proxy-agent");
const Logger = require("../utility/Logger.js");
const { getDomain } = require("tldjs");
const Try = require("../utility/Try.js");

const checks = ["checkGeoff4URLs", "checkSafeBrowsing", "checkYouTubeChannel"];
const violations = {};

class ChatService {
  constructor() {
    this.messages = new Map();
    this.urls = new Map();
    this.socksProxyAgent = new SocksProxyAgent("socks5://localhost:9050");
  }

  async runChecks(msg) {
    if (ModerationService.getPermLevel(msg.dbGuild, msg.member) > 0) {
      // Exempt mod roles to reduce false positives when quoting.
      return;
    }

    let result = false;
    for (let i = 0; i < checks.length; i++) {
      if (!result) {
        const check = checks[i];
        result = await this[check](msg);
      }
    }
  }

  async checkWord(msg) {
    const enabledChannels = msg.dbGuild.enabledChannels;
    if (enabledChannels.length === 0) {
      return;
    }
    if (msg.system) {
      return;
    }
    for (let i = 0; i < enabledChannels.length; i++) {
      if (enabledChannels[i] === msg.channel.id) {
        const alphanumeric = msg.content
          .replace(/[?!.]/g, "")
          .replace(/\s+/g, " ")
          .trim();
        const stripped = msg.content.replaceAll(" ", "");
        if (
          (!alphanumeric.includes(" ") && !alphanumeric.includes("\n")) ||
          Constants.regexes.onlyEmoji.test(stripped) ||
          Constants.regexes.onlyUnicode.test(stripped)
        ) {
          // Check for URLs.
          if (!Constants.regexes.URLs.test(msg.content)) {
            if (ModerationService.getPermLevel(msg.dbGuild, msg.member) < 1) {
              await msg.delete();
              return OneWordService.update(msg);
            }
          }
        }
      }
    }
  }

  async checkPerspective(msg) {
    if (ModerationService.getPermLevel(msg.dbGuild, msg.member) > 0) {
      // Exempt mod roles to reduce false positives when quoting.
      return;
    }

    const content = msg.content
      .replaceAll(Constants.regexes.matchEmoji, "")
      .replaceAll(Constants.regexes.matchUserMention, "")
      .replaceAll(Constants.regexes.matchRoleMention, "")
      .replaceAll(Constants.regexes.matchChannelMention, "")
      .trim();

    if (content.length > 1) {
      const results = await Perspective.analyze(content);
      const attributes = Object.keys(results);

      let displayAttributes = [];
      let displayScores = [];
      for (let i = 0; i < attributes.length; i++) {
        const attribute = attributes[i];
        const score = results[attribute].summaryScore.value;
        await Logger.log(
          `Perspective: "${content}" - { "attribute": ${attribute}, "score": ${score} }`,
          "INFO"
        );
        if (
          (attribute === "SEVERE_TOXICITY" && score > 0.88) ||
          (attribute === "IDENTITY_ATTACK" && score > 0.88) ||
          (attribute === "THREAT" && score > 0.98)
        ) {
          displayAttributes.push(attribute);
          displayScores.push(score);
        }
      }

      if (displayAttributes.length > 0 && displayScores.length > 0) {
        let message =
          `Exceeded Perspective API attribute threshold in ${msg.channel.toString()} [Jump to message](${
            msg.url
          })\n` +
          `\n${StringUtil.boldify("Content:")} ${StringUtil.removeClickableLinks(
            StringUtil.escape(msg.content)
          )}` +
          `\n${StringUtil.boldify("Attributes:")} ${displayAttributes.join(", ")}` +
          `\n${StringUtil.boldify("Scores:")} ${displayScores.join(", ")}`;
        return ModerationService.tryLogBotQueue(
          Constants.botQueueLog,
          msg.guild,
          msg.author,
          message,
          msg.channel.id,
          msg.id,
          Constants.banColor
        );
      }
    }
  }

  async checkGeoff4URLs(msg) {
    let returnValue = false;
    const urlsMatch = await this.getURLs(msg);
    if (urlsMatch == null) {
      return true;
    }
    const urls = [],
      originalURLs = [];
    for (let i = 0; i < urlsMatch.length; i++) {
      const url = new URL(urlsMatch[i]);
      const domain = getDomain(url.hostname);
      if (
        domain != null &&
        !Constants.allowListedURLs.includes(domain) &&
        !Constants.allowListedThirdParty.includes(domain)
      ) {
        urls.push((url.hostname + url.pathname).replaceAll(";", ""));
        originalURLs.push(urlsMatch[i]);
      }
    }

    if (urls.length > 0) {
      const options = {
        url: "http://localhost:8080/",
        headers: {
          Type: "geoff4URLs",
          Content: encodeURIComponent(urls.join(";")),
        },
      };

      const response = await axios(options).catch((err) => Logger.handleError(err));
      let content = msg.content.replaceAll(/(https?:\/\/)/g, "");
      let scores = [],
        displayURLs = [];
      let f1Logs = false;
      for (let i = 0; i < urls.length; i++) {
        const score = response.data[i];
        if (score > 0.85) {
          await Try(msg.delete());
          f1Logs = true;
        }
        if (score > 0.5) {
          const url = urls[i];
          const strippedUrl = StringUtil.stripURL(url);
          let originalURL = new URL(this.getOriginalURL(originalURLs[i]));
          originalURL = StringUtil.stripURL(
            originalURL.hostname + originalURL.pathname
          );
          content = StringUtil.removeClickableLinks(StringUtil.escape(msg.content));
          if (originalURL === strippedUrl) {
            displayURLs.push(strippedUrl);
          } else {
            displayURLs.push(originalURL + " -> " + strippedUrl);
          }
          scores.push(score);
        }
      }
      if (scores.length > 0) {
        if (f1Logs) {
          const messages = await msg.channel.messages.fetch({ limit: 2 });
          const aboveMessage = messages.last().url;
          let message =
            `Exceeded _geoff4URLs_ threshold in ${msg.channel.toString()} [Jump to message above](${aboveMessage})\n` +
            `\n${StringUtil.boldify("Content:")} ${content}` +
            `\n${StringUtil.boldify("URLs:")} ${displayURLs.toString()}` +
            `\n${StringUtil.boldify("Scores:")} ${scores.toString()}`;
          await ModerationService.tryLogBotQueue(
            Constants.botQueueLog,
            msg.guild,
            msg.author,
            message,
            msg.channel.id,
            msg.id,
            Constants.kickColor
          );
          await this.checkViolations(msg);
          returnValue = true;
        }
        let message =
          `Exceeded _geoff4URLs_ threshold in ${msg.channel.toString()} [Jump to message](${
            msg.url
          })\n` +
          `\n${StringUtil.boldify("Content:")} ${content}` +
          `\n${StringUtil.boldify("URLs:")} ${StringUtil.removeClickableLinks(
            displayURLs.toString()
          )}` +
          `\n${StringUtil.boldify("Scores:")} ${scores.toString()}`;
        await ModerationService.tryLogBotQueue(
          Constants.botTesting,
          msg.guild,
          msg.author,
          message,
          msg.channel.id,
          msg.id,
          Constants.warnColor,
          false,
          false
        );
      }
    }
    return returnValue;
  }

  async checkSafeBrowsing(msg) {
    const urls = await this.getURLs(msg);
    // for (let i = 0; i < urls.length; i++) {
    //   urls.push(new URL(urls[i]).hostname);
    // }
    if (urls.length > 0) {
      const results = await SafeBrowsing.lookup(urls);
      if (results) {
        await Try(msg.delete());
        const messages = await msg.channel.messages.fetch({ limit: 2 });
        const aboveMessage = messages.last().url;
        let message =
          `Posted a link which is in Safe Browsing in ${msg.channel.toString()} [Jump to message above](${aboveMessage})\n` +
          `\n${StringUtil.boldify("Content:")} ${StringUtil.removeClickableLinks(
            StringUtil.escape(msg.content)
          )}` +
          `\n${StringUtil.boldify("Type:")} ${results.threatType}` +
          `\n${StringUtil.boldify("Platform:")} ${results.platformType}`;
        await ModerationService.tryLogBotQueue(
          Constants.botQueueLog,
          msg.guild,
          msg.author,
          message,
          msg.channel.id,
          msg.id,
          Constants.kickColor
        );
        await this.checkViolations(msg);
        return true;
      }
    }
    return false;
  }

  async checkYouTubeChannel(msg) {
    const urls = await this.getURLs(msg);
    let ids = [];
    for (let i = 0; i < urls.length; i++) {
      if (Constants.regexes.youtubeVideo.test(urls[i])) {
        ids.push(urls[i].match(Constants.regexes.youtubeVideo)[1]);
      }
    }
    if (ids.length > 0) {
      const results = await YouTubeData.lookup(ids);
      const blocklistedChannels = msg.dbGuild.youtubeChannels.blocklisted;
      if (results) {
        for (let i = 0; i < results.length; i++) {
          if (blocklistedChannels.includes(results[i].channelId)) {
            msg.delete();
            await msg.sender.dm(
              msg.author,
              `The YouTube channel ${results[i].channelId} is currently blocklisted.`,
              msg.channel,
              { color: Constants.errorColor }
            );
            return true;
          }
        }
      }
    }
    return false;
  }

  async getURLs(msg) {
    const pattern = Constants.regexes.URLs;
    const urlsMatch = [...new Set(msg.content.match(pattern))];
    if (urlsMatch.length > 20) {
      msg.delete();
      return;
    }
    if (urlsMatch.length > 0) {
      let results = [];
      for (let i = 0; i < urlsMatch.length; i++) {
        const url = urlsMatch[i];
        const urlStripped = StringUtil.stripURL(url);
        const cachedResult = this.urls.get(urlStripped);
        if (cachedResult) {
          results.push(cachedResult);
        } else {
          const responseUrl = (await this.getLocation(url)) ?? url;
          const urlStripped = StringUtil.stripURL(url);
          this.urls.set(urlStripped, responseUrl);
          if (this.urls.size > 1000) {
            // If the entries exceed the maximum value, remove the earliest item added to the cache.
            this.urls.delete(this.urls.keys().next().value);
          }
          results.push(responseUrl);
        }
      }
      results = [...new Set(results)];
      return results;
    }
  }

  async getLocation(url) {
    const response = await axios
      .head(url, {
        httpAgent: this.socksProxyAgent,
        httpsAgent: this.socksProxyAgent,
        timeout: 10000,
      })
      .catch((err) => Logger.log(err.message, "DEBUG")); // Use a proxy to prevent bad actors from simply serving a different response to our IP address.
    return response?.request?.res?.responseUrl;
  }

  getOriginalURL(url) {
    for (let [key, value] of this.urls) {
      if (value === url) {
        return key;
      }
    }
  }

  stripURL(url) {
    url = url.toLowerCase();
    if (url.endsWith("/")) {
      url = url.substring(0, url.length - 1);
    }
    return url;
  }

  removeClickableLinks(content) {
    const pattern = Constants.regexes.URLs;
    const urls = content.match(pattern);
    if (urls) {
      urls.forEach((url) => {
        content = content.replace(url, StringUtil.unlinkify(url));
      });
    }
    return content;
  }

  async checkViolations(msg) {
    const violation = violations[msg.author.id];
    if (violation) {
      if (violation.violationStart + 300000 > Date.now()) {
        violations[msg.author.id].violationCount++;
        if (violation.violationCount === 3) {
          const role = msg.guild.roles.cache.get(msg.dbGuild.roles.muted);
          msg.member.roles.add(role);
          return ModerationService.tryLogBotQueue(
            Constants.botQueueLog,
            msg.guild,
            msg.author,
            `Posted 3 or more violations within 5 minutes so I muted them.`,
            msg.channel.id,
            msg.id,
            Constants.banColor,
            true,
            true
          );
        }
      } else {
        violations[msg.author.id] = {
          violationStart: Date.now(),
          violationCount: 1,
        };
      }
    } else {
      violations[msg.author.id] = {
        violationStart: Date.now(),
        violationCount: 1,
      };
    }
  }
}

module.exports = new ChatService();
