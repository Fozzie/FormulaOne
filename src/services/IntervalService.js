const path = require("path");
const RequireAll = require("patron").RequireAll;

class IntervalService {
  initiate() {
    RequireAll(path.join(__dirname, "../intervals"));
  }
}

module.exports = new IntervalService();
