const Constants = require("../utility/Constants.js");
const Sender = require("../utility/Sender.js");
const StringUtil = require("../utility/StringUtil.js");
const db = require("../database/index.js");
const Discord = require("discord.js");

class ModerationService {
  getPermLevel(dbGuild, member) {
    if (member.guild.ownerID === member.id) {
      return 3;
    }

    if (member.id === "259745940972240896") {
      return 3;
    }

    let permLevel = 0;

    for (const modRole of dbGuild.roles.mod.sort(
      (a, b) => a.permissionLevel - b.permissionLevel
    )) {
      if (
        member.guild.roles.cache.has(modRole.id) &&
        member.roles.cache.has(modRole.id)
      ) {
        permLevel = modRole.permissionLevel;
      }
    }

    return member.permissions.has("ADMINISTRATOR") && permLevel < 2 ? 2 : permLevel;
  }

  getPermLevelStr(dbGuild, member) {
    const permL = this.getPermLevel(dbGuild, member);
    if (permL === 1) {
      return "Marshal";
    }
    if (permL === 2) {
      return "Steward";
    }
    if (permL === 3) {
      return "Server Admin";
    }
    return "Member";
  }

  async tryLogAnything(dbGuild, guild, author, message, footer, buttons = []) {
    const logChannel = await guild.channels.resolve(Constants.modLog);
    const embedOptions = {
      timestamp: true,
    };
    if (author != null) {
      embedOptions.author = {
        name: author.tag,
        icon_url: author.displayAvatarURL(),
      };
    }
    if (footer != null) {
      embedOptions.footer = {
        text: footer,
      };
    }
    const messageOptions = {};
    if (buttons.length > 0) {
      messageOptions.components = buttons.map((b) => ({ type: 1, components: b }));
    }

    return Sender.send(logChannel, message, embedOptions, messageOptions);
  }

  async tryLogBotQueue(
    logChannel,
    guild,
    author,
    message,
    channelId,
    messageId,
    color,
    useButtons = true,
    autoMute = false,
    banButton = true,
    mention = false
  ) {
    logChannel = await guild.channels.resolve(logChannel);
    let buttons;
    if (useButtons) {
      buttons = [
        [
          new Discord.MessageButton()
            .setCustomId("userid-" + author.id)
            .setLabel("User ID")
            .setStyle("SECONDARY"),
          new Discord.MessageButton()
            .setCustomId("punish-" + author.id + "-" + channelId + "-" + messageId)
            .setLabel("Punish")
            .setStyle("DANGER"),
        ],
      ];

      if (banButton) {
        buttons[0].push(
          new Discord.MessageButton()
            .setCustomId("ban-" + author.id + "-unconfirmed")
            .setLabel("Ban")
            .setStyle("DANGER")
        );
      } else {
        buttons[0].push(
          new Discord.MessageButton()
            .setCustomId("escalate-" + author.id + "-" + channelId + "-" + messageId)
            .setLabel("Escalate")
            .setStyle("PRIMARY")
        );
      }
      if (autoMute) {
        buttons[0].push(
          new Discord.MessageButton()
            .setCustomId("unmute-" + author.id + "-unconfirmed")
            .setLabel("Unmute")
            .setStyle("SUCCESS")
        );
      }
      buttons[0].push(
        new Discord.MessageButton()
          .setCustomId("ignore-" + author.id)
          .setLabel("Ignore")
          .setStyle("SECONDARY")
      );
    }

    const options = {
      color: color,
      footer: {
        text: "User ID: " + author.id + " - Message ID: " + messageId,
      },
      timestamp: true,
    };
    options.author = {
      name: author.tag,
      icon_url: author.displayAvatarURL(),
    };

    const messageOptions = {};
    if (useButtons) {
      messageOptions.components = buttons.map((b) => ({
        type: 1,
        components: b,
      }));
    }
    if (autoMute || mention) {
      messageOptions.content = "<@&738665034359767060>";
    }

    await Sender.send(logChannel, message, options, messageOptions);
  }

  async tryLogSafeBrowsing(dbGuild, guild, author, message, messageId) {
    const logChannel = await guild.channels.resolve(Constants.modLog);
    const options = {
      color: Constants.banColor,
      footer: {
        text: "User ID: " + author.id + " - Message ID: " + messageId,
      },
      timestamp: true,
    };
    options.author = {
      name: author.tag,
      icon_url: author.displayAvatarURL(),
    };

    return Sender.send(logChannel, message, options);
  }

  async tryModLog(
    dbGuild,
    guild,
    action,
    color,
    reason = "",
    moderator = null,
    user = null,
    extraInfoType = "",
    extraInfo = "",
    extraInfoType2 = "",
    extraInfo2 = ""
  ) {
    const channel = await guild.channels.resolve(Constants.modLog);
    let caseNum = dbGuild.caseNumber;

    const buttons = [];
    const options = {
      color: color,
      footer: {
        text: "Case #" + caseNum,
      },
      timestamp: true,
    };

    if (moderator != null) {
      options.author = {
        name: moderator.tag,
        icon_url: moderator.displayAvatarURL(),
      };
    }

    let description = "**Action:** " + action + "\n";

    if (user != null) {
      description +=
        "**User:** " + StringUtil.escape(user.tag) + " (" + user.id + ")\n";
      buttons.push([
        new Discord.MessageButton()
          .setCustomId("userid-" + user.id)
          .setLabel("User ID")
          .setStyle("SECONDARY"),
      ]);
    }

    if (StringUtil.isNullOrWhiteSpace(reason) === false) {
      description += "**Reason:** " + StringUtil.escape(reason) + "\n";
    }

    if (StringUtil.isNullOrWhiteSpace(extraInfo) === false) {
      description +=
        "**" +
        extraInfoType +
        ":** " +
        StringUtil.removeClickableLinks(StringUtil.escape(extraInfo)) +
        "\n";
    }

    if (StringUtil.isNullOrWhiteSpace(extraInfo2) === false) {
      description +=
        "**" +
        extraInfoType2 +
        ":** " +
        StringUtil.removeClickableLinks(StringUtil.escape(extraInfo2)) +
        "\n";
    }

    await db.guildRepo.upsertGuild(dbGuild.guildId, {
      $inc: { caseNumber: 1 },
    });
    return Sender.send(channel, description, options, {
      components: buttons.map((b) => ({ type: 1, components: b })),
    });
  }

  async tryReportLog(guild, reason, author, channel, color) {
    const logChannel = await guild.channels.resolve("294051298733719552");

    const options = {
      color: color,
      timestamp: true,
    };

    if (author != null) {
      options.author = {
        name: author.tag,
        icon_url: author.displayAvatarURL(),
      };
    }
    let description = "**Message:** " + reason + "\n**Channel:** " + channel;
    if (logChannel == null) {
      Sender.send(
        await guild.channels.resolve("447397947261452288"),
        "Failed to send report to the Mod channel.\n" + description
      );
    }
    await logChannel.send("@here New report.");
    return Sender.send(logChannel, description, options);
  }
}

module.exports = new ModerationService();
