const { TypeReader, TypeReaderResult } = require("patron");
const Constants = require("../utility/Constants.js");

class TextChannels extends TypeReader {
  constructor() {
    super({ type: "textchannels" });
  }

  async read(input, command, msg, arg) {
    const inputs = input.split(" ");

    const matches = [];
    inputs.forEach((channelInput) => {
      if (Constants.regexes.id.test(channelInput)) {
        const textChannel = msg.guild.channels.cache.get(channelInput);

        if (textChannel != null) {
          return matches.push(textChannel);
        }
      }

      const textChannelMatches = channelInput.match(
        Constants.regexes.onlyChannelMention
      );
      if (textChannelMatches) {
        const textChannel = msg.guild.channels.cache.get(textChannelMatches[1]);

        if (textChannel != null) {
          return matches.push(textChannel);
        }
      }

      channelInput = channelInput.toLowerCase().trim();
      const textChannel = msg.guild.channels.cache.find(
        (channel) =>
          channel.name.toLowerCase() === channelInput && channel.type === "GUILD_TEXT"
      );

      if (textChannel != null) {
        return matches.push(textChannel);
      }
    });

    if (matches.length > 0) {
      return TypeReaderResult.fromSuccess(matches);
    }

    return TypeReaderResult.fromFailure(command, "No text channels found.");
  }
}

module.exports = new TextChannels();
