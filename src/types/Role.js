const { TypeReader, TypeReaderResult } = require("patron");
const Constants = require("../utility/Constants.js");
const ReaderUtil = require("../utility/ReaderUtil.js");

class Role extends TypeReader {
  constructor() {
    super({ type: "role" });
  }

  async read(input, command, msg, arg) {
    const firstInput = input.split(" ")[0];
    if (Constants.regexes.id.test(firstInput)) {
      const role = msg.guild.roles.cache.get(firstInput);

      if (role != null) {
        return TypeReaderResult.fromSuccess(role);
      }
    }

    input = input.toLowerCase().trim();
    const role = msg.guild.roles.cache.find(
      (role) => role.name.toLowerCase() === input
    );

    if (role != null) {
      return TypeReaderResult.fromSuccess(role);
    }

    const filter = msg.guild.roles.cache.filter((role) =>
      role.name.toLowerCase().startsWith(input)
    );
    const result = ReaderUtil.handleMatches(msg, filter);
    if (result.match) {
      return TypeReaderResult.fromSuccess(filter.first());
    }

    return TypeReaderResult.fromFailure(command, result.error ?? "Role not found.");
  }
}

module.exports = new Role();
