const { TypeReader, TypeReaderResult } = require("patron");
const Constants = require("../utility/Constants.js");
const ReaderUtil = require("../utility/ReaderUtil.js");
const Try = require("../utility/Try.js");

class User extends TypeReader {
  constructor() {
    super({ type: "user" });
  }

  async read(input, command, msg, arg) {
    const firstInput = input.split(" ")[0];
    if (Constants.regexes.id.test(firstInput)) {
      const user = await Try(msg.client.users.fetch(firstInput), true);

      if (user) {
        return TypeReaderResult.fromSuccess(user);
      }
    }

    const userMatches = firstInput.match(Constants.regexes.onlyUserMention);
    if (userMatches) {
      const user = await Try(msg.client.users.fetch(userMatches[1]), true);

      if (user) {
        return TypeReaderResult.fromSuccess(user);
      }
    }

    input = input.toLowerCase().trim();
    const user = msg.guild.members.cache.find(
      (member) => member.user.tag.toLowerCase() === input
    )?.user;

    if (user != null) {
      return TypeReaderResult.fromSuccess(user);
    }

    const filter = msg.guild.members.cache.filter((member) =>
      member.user.tag.toLowerCase().startsWith(input)
    );
    const result = ReaderUtil.handleMatches(msg, filter, true);
    if (result.match) {
      return TypeReaderResult.fromSuccess(filter.first().user);
    }

    return TypeReaderResult.fromFailure(command, result.error ?? "User not found.");
  }
}

module.exports = new User();
