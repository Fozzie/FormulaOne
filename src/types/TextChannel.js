const { TypeReader, TypeReaderResult } = require("patron");
const Constants = require("../utility/Constants.js");
const ReaderUtil = require("../utility/ReaderUtil.js");

class TextChannel extends TypeReader {
  constructor() {
    super({ type: "textchannel" });
  }

  async read(input, command, msg, arg) {
    const firstInput = input.split(" ")[0];
    if (Constants.regexes.id.test(firstInput)) {
      const textChannel = msg.guild.channels.cache.get(firstInput);

      if (textChannel != null) {
        return TypeReaderResult.fromSuccess(textChannel);
      }
    }

    const textChannelMatches = firstInput.match(Constants.regexes.onlyChannelMention);
    if (textChannelMatches) {
      const textChannel = msg.guild.channels.cache.get(textChannelMatches[1]);

      if (textChannel != null) {
        return TypeReaderResult.fromSuccess(textChannel);
      }
    }

    input = input.toLowerCase().trim();
    const textChannel = msg.guild.channels.cache.find(
      (channel) => channel.name.toLowerCase() === input && channel.type === "GUILD_TEXT"
    );

    if (textChannel != null) {
      return TypeReaderResult.fromSuccess(textChannel);
    }

    const filter = msg.guild.channels.cache.filter(
      (channel) =>
        channel.name.toLowerCase().startsWith(input) && channel.type === "GUILD_TEXT"
    );
    const result = ReaderUtil.handleMatches(msg, filter);
    if (result.match) {
      return TypeReaderResult.fromSuccess(filter.first());
    }

    return TypeReaderResult.fromFailure(
      command,
      result.error ?? "Text channel not found."
    );
  }
}

module.exports = new TextChannel();
