const { TypeReader, TypeReaderResult } = require("patron");
const Constants = require("../utility/Constants.js");
const ReaderUtil = require("../utility/ReaderUtil.js");
const Try = require("../utility/Try.js");

class Member extends TypeReader {
  constructor() {
    super({ type: "member" });
  }

  async read(input, command, msg, arg) {
    const firstInput = input.split(" ")[0];
    if (Constants.regexes.id.test(firstInput)) {
      const member = await Try(msg.guild.members.fetch(firstInput), true);

      if (member) {
        return TypeReaderResult.fromSuccess(member);
      }
    }

    const userMatches = firstInput.match(Constants.regexes.onlyUserMention);
    if (userMatches) {
      const member = await Try(msg.guild.members.fetch(userMatches[1]), true);

      if (member) {
        return TypeReaderResult.fromSuccess(member);
      }
    }

    input = input.toLowerCase().trim();
    const member = msg.guild.members.cache.find(
      (member) => member.user.tag.toLowerCase() === input
    );

    if (member != null) {
      return TypeReaderResult.fromSuccess(member);
    }

    const filter = msg.guild.members.cache.filter((member) =>
      member.user.tag.toLowerCase().startsWith(input)
    );
    const result = ReaderUtil.handleMatches(msg, filter, true);
    if (result.match) {
      return TypeReaderResult.fromSuccess(filter.first());
    }

    return TypeReaderResult.fromFailure(command, result.error ?? "Member not found.");
  }
}

module.exports = new Member();
