const { TypeReader, TypeReaderResult } = require("patron");
const Constants = require("../utility/Constants.js");
const ReaderUtil = require("../utility/ReaderUtil.js");
const Try = require("../utility/Try.js");

class TextChannel extends TypeReader {
  constructor() {
    super({ type: "banneduser" });
  }

  async read(input, command, msg, arg) {
    const firstInput = input.split(" ")[0];
    if (Constants.regexes.id.test(firstInput)) {
      const ban = await Try(msg.guild.bans.fetch(firstInput), true);
      if (ban) {
        const user = await Try(msg.client.users.fetch(firstInput), true);

        if (user) {
          return TypeReaderResult.fromSuccess(user);
        }
      }
    }

    const userMatches = firstInput.match(Constants.regexes.onlyUserMention);
    if (userMatches) {
      const ban = await Try(msg.guild.bans.fetch(firstInput), true);
      if (ban) {
        const user = await Try(msg.client.users.fetch(userMatches[1]), true);

        if (user) {
          return TypeReaderResult.fromSuccess(user);
        }
      }
    }

    input = input.toLowerCase().trim();
    const bans = await msg.guild.bans.fetch();
    const guildBan = bans.find((guildban) => guildban.user.tag.toLowerCase() === input);

    if (guildBan != null) {
      return TypeReaderResult.fromSuccess(guildBan.user);
    }

    const filter = bans.filter((guildban) =>
      guildban.user.tag.toLowerCase().startsWith(input)
    );
    const result = ReaderUtil.handleMatches(msg, filter, true);
    if (result.match) {
      return TypeReaderResult.fromSuccess(filter.first().user);
    }

    return TypeReaderResult.fromFailure(
      command,
      result.error ?? "Banned user not found."
    );
  }
}

module.exports = new TextChannel();
