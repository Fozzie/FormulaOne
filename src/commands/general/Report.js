const patron = require("patron");

class Report extends patron.Command {
  constructor() {
    super({
      names: ["report"],
      group: "general",
      description: "Send a report to the Moderators.",
      arguments: [
        new patron.Argument({
          name: "reason",
          key: "reason",
          type: "string",
          example: "Non constructive message.",
          remainder: true,
        }),
      ],
    });
  }

  async run(msg, args) {
    await msg.delete();
    return msg.sender.dm(
      msg.author,
      "To report a user, use `?report <Reason>` or right click on a message -> `Apps` -> `Report`",
      msg.channel
    );
  }
}

module.exports = new Report();
