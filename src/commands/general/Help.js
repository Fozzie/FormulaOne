const patron = require("patron");
const StringUtil = require("../../utility/StringUtil.js");
const Constants = require("../../utility/Constants.js");
const ModerationService = require("../../services/ModerationService.js");

class Help extends patron.Command {
  constructor() {
    super({
      names: ["help", "commands", "cmds", "cmd"],
      group: "general",
      description: "View all of the command information.",
      arguments: [
        new patron.Argument({
          name: "command",
          key: "command",
          type: "string",
          defaultValue: patron.ArgumentDefault.Member,
          example: "commands",
          remainder: true,
        }),
      ],
    });
  }

  async run(msg, args) {
    if (StringUtil.isNullOrWhiteSpace(args.command)) {
      const permLevel = ModerationService.getPermLevel(msg.dbGuild, msg.member);
      const displayGroups = {
        administrator: "Stewards",
        botowners: "Bot Owners",
        f4: "F4",
        general: "General",
        helper: "Helper",
        moderation: "Marshals",
        owner: "Admins",
      };
      const groups = [...msg.client.registry.groups.keys()];
      let commands = [];
      msg.client.registry.commands.forEach((command, commandName) => {
        // Remove duplicates
        if (command.names[0] === commandName) {
          commands.push(command);
        }
      });

      let message = "";
      groups.forEach((group) => {
        if (
          (group === "helper" && msg.member.roles.cache.has(Constants.roles.helper)) ||
          (group === "moderation" && permLevel < 1) ||
          (group === "administrator" && permLevel < 2) ||
          ((group === "owner" || group === "botowners") && permLevel < 3)
        ) {
          return;
        }
        const groupCommands = commands.filter((command) => command.group === group);
        if (groupCommands.length > 0) {
          message += `**${displayGroups[group]}**\n`;
          groupCommands.forEach(
            (command) =>
              (message +=
                "`" +
                Constants.prefix +
                command.names[0] +
                "` - " +
                command.description +
                "\n")
          );
          message += "\n";
        }
      });

      message +=
        "\nYou can find a specific command's information by using `$help [Command]`\n\n**Formula One** is made by **Fozzie#0001**.";
      await msg.sender.dm(msg.author, message, msg.channel);
      return msg.sender.reply("Successfully DMed you all command information.");
    } else {
      args.command = args.command.startsWith(Constants.prefix)
        ? args.command.slice(Constants.prefix.length)
        : args.command;

      const lowerInput = args.command.toLowerCase();

      const command = msg.client.registry.commands.get(lowerInput);

      if (command == null) {
        return msg.sender.reply("This command does not exist.", {
          color: Constants.errorColor,
        });
      }

      return msg.sender.send(
        "**Description:** " +
          command.description +
          "\n**Usage:** `" +
          Constants.prefix +
          command.getUsage() +
          "`\n**Example:** `" +
          Constants.prefix +
          command.getExample() +
          "`",
        { title: StringUtil.upperFirstChar(command.names[0]) }
      );
    }
  }
}

module.exports = new Help();
