const patron = require("patron");
const Constants = require("../../utility/Constants.js");
const StringUtil = require("../../utility/StringUtil.js");

class Jezza extends patron.Command {
  constructor() {
    super({
      names: ["jezza"],
      group: "administrator",
      description: "Applies light mode to a user.",
      arguments: [
        new patron.Argument({
          name: "member",
          key: "member",
          type: "member",
          example: "Jizza#0005",
          remainder: true,
        }),
      ],
    });
  }

  async run(msg, args) {
    await args.member.roles.remove([
      Constants.buttonRoles.ferrari,
      Constants.buttonRoles.mercedes,
      Constants.buttonRoles.redbull,
      Constants.buttonRoles.alpine,
      Constants.buttonRoles.haas,
      Constants.buttonRoles.astonmartin,
      Constants.buttonRoles.mclaren,
      Constants.buttonRoles.alphatauri,
      Constants.buttonRoles.alfaromeo,
      Constants.buttonRoles.williams,
    ]);
    await args.member.roles.add([
      Constants.buttonRoles.ferrari,
      Constants.buttonRoles.mercedes,
      Constants.buttonRoles.redbull,
      Constants.buttonRoles.alpine,
      Constants.buttonRoles.haas,
      Constants.buttonRoles.astonmartin,
      Constants.buttonRoles.mclaren,
      Constants.buttonRoles.alphatauri,
      Constants.buttonRoles.alfaromeo,
      Constants.buttonRoles.williams,
    ]);
    return msg.sender.reply(
      "Successfully applied all team roles to " +
        StringUtil.boldify(args.member.user.tag) +
        "."
    );
  }
}

module.exports = new Jezza();
