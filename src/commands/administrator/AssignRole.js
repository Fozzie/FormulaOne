const patron = require("patron");
const Constants = require("../../utility/Constants.js");

class AssignRole extends patron.Command {
  constructor() {
    super({
      names: ["rank", "team", "role", "assignrole", "unassignrole"],
      group: "administrator",
      description: "Assign or unassign an assignable role.",
      arguments: [
        new patron.Argument({
          name: "role",
          key: "role",
          type: "role",
          example: "Ferrari",
          preconditions: ["hierarchy"],
          remainder: true,
        }),
      ],
    });
  }

  async run(msg, args) {
    if (!msg.dbGuild.roles.assignable.includes(args.role.id)) {
      return msg.sender.reply(args.role.toString() + " is not assignable.", {
        color: Constants.errorColor,
      });
    }

    if (msg.member.roles.cache.has(args.role.id)) {
      await msg.member.roles.remove(args.role);
      return msg.sender.reply("Successfully unassigned role " + args.role.toString());
    } else {
      await msg.member.roles.add(args.role);
      return msg.sender.reply("Successfully assigned role " + args.role.toString());
    }
  }
}

module.exports = new AssignRole();
