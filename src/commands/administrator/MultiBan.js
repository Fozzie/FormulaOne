const patron = require("patron");
const ModerationService = require("../../services/ModerationService.js");
const client = require("../../singletons/client.js");

class Multiban extends patron.Command {
  constructor() {
    super({
      names: ["multiban", "mban"],
      group: "administrator",
      description: "Ban multiple ids at once, separate them with spaces.",
      arguments: [
        new patron.Argument({
          name: "ids",
          key: "ids",
          type: "string",
          example: "475013414566232094 467574105722322944 467573667249913878",
          remainder: true,
        }),
      ],
    });
  }

  async run(msg, args) {
    const users = args.ids.split(" ");
    let message = "";
    let successfulBans = "";
    for (let i = 0; i < users.length; i++) {
      const removedW = users[i].replace(/\s/g, "").toString();
      let specificUser;
      try {
        specificUser = await client.users.fetch(removedW);
      } catch (e) {
        continue;
      }
      if (msg.guild.members.cache.has(removedW)) {
        const specificMember = msg.guild.members.cache.get(removedW);
        if (ModerationService.getPermLevel(msg.dbGuild, specificMember) >= 1) {
          message += `Could not ban ${removedW} (${specificMember.user.tag}) as they are a Moderator.`;
          continue;
        }
      }
      await msg.guild.members.ban(specificUser, {
        reason: `(${msg.author.tag}) Multiban`,
      });
      successfulBans += `${removedW} (${specificUser.tag})\n`;
      message += `Banned ${removedW} (${specificUser.tag})\n`;
    }
    if (successfulBans !== "") {
      await ModerationService.tryLogAnything(
        msg.dbGuild,
        msg.guild,
        msg.author,
        `Banned multiple IDs: \n${successfulBans}`,
        null
      );
    }
    return msg.sender.send(message, { title: "Multiban Results" });
  }
}

module.exports = new Multiban();
