const patron = require("patron");
const Constants = require("../../utility/Constants.js");
const db = require("../../database");

class RemoveAssignableRole extends patron.Command {
  constructor() {
    super({
      names: ["removerole", "removerank", "removeassignablerole", "removeassignrole"],
      group: "administrator",
      description: "Remove an assignable role from the list.",
      arguments: [
        new patron.Argument({
          name: "role",
          key: "role",
          type: "role",
          example: "Ferrari",
          preconditions: ["hierarchy"],
          remainder: true,
        }),
      ],
    });
  }

  async run(msg, args) {
    if (!msg.dbGuild.roles.assignable.includes(args.role.id)) {
      return msg.sender.reply(
        "The " + args.role.toString() + " role is not assignable.",
        { color: Constants.errorColor }
      );
    }

    await db.guildRepo.upsertGuild(
      msg.guild.id,
      new db.updates.Pull("roles.assignable", args.role.id)
    );
    return msg.sender.reply(
      "Successfully removed the assignable role for " + args.role.toString() + "."
    );
  }
}

module.exports = new RemoveAssignableRole();
