const patron = require("patron");
const Constants = require("../../utility/Constants.js");
const db = require("../../database");

class AddYouTubeChannel extends patron.Command {
  constructor() {
    super({
      names: ["addyoutubechannel"],
      group: "administrator",
      description: "Add a YouTube channel to the blocklist.",
      arguments: [
        new patron.Argument({
          name: "channel",
          key: "channel",
          type: "string",
          example: "UCBR8-60-B28hp2BmDPdntcQ",
          remainder: true,
        }),
      ],
    });
  }

  async run(msg, args) {
    if (!Constants.regexes.youtubeChannel.test(args.channel)) {
      return msg.sender.reply(
        "That is not a valid base64 channel ID. Make sure the channel looks like `UCBR8-60-B28hp2BmDPdntcQ` and is not a personalised ID.",
        { color: Constants.errorColor }
      );
    }

    if (msg.dbGuild.youtubeChannels.blocklisted.includes(args.channel)) {
      return msg.sender.reply(
        "The channel " + args.channel + " is already blocklisted.",
        { color: Constants.errorColor }
      );
    }

    await db.guildRepo.upsertGuild(
      msg.guild.id,
      new db.updates.Push("youtubeChannels.blocklisted", args.channel)
    );
    return msg.sender.reply(
      "Successfully blocklisted the " + args.channel + " channel."
    );
  }
}

module.exports = new AddYouTubeChannel();
