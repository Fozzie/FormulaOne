const patron = require("patron");
const Constants = require("../../utility/Constants.js");
const db = require("../../database");

class AddAssignableRole extends patron.Command {
  constructor() {
    super({
      names: ["addrole", "addrank", "addassignablerole", "addassignrole"],
      group: "administrator",
      description: "Add an assignable role to the list.",
      arguments: [
        new patron.Argument({
          name: "role",
          key: "role",
          type: "role",
          example: "Ferrari",
          preconditions: ["hierarchy"],
          remainder: true,
        }),
      ],
    });
  }

  async run(msg, args) {
    if (msg.dbGuild.roles.assignable.includes(args.role.id)) {
      return msg.sender.reply(
        "The " + args.role.toString() + " role is already assignable.",
        { color: Constants.errorColor }
      );
    }

    await db.guildRepo.upsertGuild(
      msg.guild.id,
      new db.updates.Push("roles.assignable", args.role.id)
    );
    return msg.sender.reply(
      "Successfully made the " + args.role.toString() + " role assignable."
    );
  }
}

module.exports = new AddAssignableRole();
