const patron = require("patron");
const Constants = require("../../utility/Constants.js");
const db = require("../../database");

class RemoveYouTubeChannel extends patron.Command {
  constructor() {
    super({
      names: ["removeyoutubechannel"],
      group: "administrator",
      description: "Remove a YouTube channel from the blocklist.",
      arguments: [
        new patron.Argument({
          name: "channel",
          key: "channel",
          type: "string",
          example: "UCBR8-60-B28hp2BmDPdntcQ",
          remainder: true,
        }),
      ],
    });
  }

  async run(msg, args) {
    if (!Constants.regexes.youtubeChannel.test(args.channel)) {
      return msg.sender.reply(
        "That is not a valid base64 channel ID. Make sure the channel looks like `UCBR8-60-B28hp2BmDPdntcQ` and is not a personalised ID.",
        { color: Constants.errorColor }
      );
    }
    if (!msg.dbGuild.youtubeChannels.blocklisted.includes(args.channel)) {
      return msg.sender.reply("The channel " + args.channel + " is not blocklisted.", {
        color: Constants.errorColor,
      });
    }

    await db.guildRepo.upsertGuild(
      msg.guild.id,
      new db.updates.Pull("youtubeChannels.blocklisted", args.channel)
    );
    return msg.sender.reply(
      "Successfully removed the " + args.channel + " channel from the blocklist."
    );
  }
}

module.exports = new RemoveYouTubeChannel();
