const patron = require("patron");
const StringUtil = require("../../utility/StringUtil.js");
const ModerationService = require("../../services/ModerationService.js");
const Constants = require("../../utility/Constants.js");
const db = require("../../database/index.js");

class Ban extends patron.Command {
  constructor() {
    super({
      names: ["ban"],
      group: "administrator",
      description: "Ban a user.",
      arguments: [
        new patron.Argument({
          name: "user",
          key: "user",
          type: "user",
          example: "coco-bun#6681",
          preconditions: ["nomoderator"],
        }),
        new patron.Argument({
          name: "reason",
          key: "reason",
          type: "string",
          example: "Posting an IP logger.",
          remainder: true,
        }),
      ],
    });
  }

  async run(msg, args) {
    if (await db.banRepo.anyBan(args.user.id, msg.guild.id)) {
      await db.banRepo.deleteBan(args.user.id, msg.guild.id);
    }
    if (msg.guild.members.cache.has(args.user.id)) {
      await msg.sender.dm(
        args.user,
        "A moderator has banned you" +
          (StringUtil.isNullOrWhiteSpace(args.reason)
            ? "."
            : " for the reason: " + args.reason + "."),
        msg.channel
      );
      await db.userRepo.upsertUser(args.user.id, msg.guild.id, {
        $inc: { bans: 1 },
      });
    }
    await msg.guild.members.ban(args.user, {
      reason: `(${msg.author.tag}) ${args.reason}`,
    });
    await msg.sender.reply(
      "Successfully banned " + StringUtil.boldify(args.user.tag) + "."
    );
    await db.userRepo.upsertUser(
      args.user.id,
      msg.guild.id,
      new db.updates.Push("punishments", {
        date: Date.now(),
        escalation: "Ban",
        reason: args.reason,
        mod: msg.author.tag,
        channelId: msg.channel.id,
      })
    );
    return ModerationService.tryModLog(
      msg.dbGuild,
      msg.guild,
      "Ban",
      Constants.banColor,
      args.reason,
      msg.author,
      args.user
    );
  }
}

module.exports = new Ban();
