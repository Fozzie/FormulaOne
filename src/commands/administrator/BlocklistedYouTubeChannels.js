const patron = require("patron");
const Constants = require("../../utility/Constants.js");

class BlocklistedYouTubeChannels extends patron.Command {
  constructor() {
    super({
      names: ["blocklistedyoutubechannels"],
      group: "administrator",
      description: "View all of the blocklisted YouTube channels.",
    });
  }

  async run(msg, args) {
    if (msg.dbGuild.youtubeChannels.blocklisted.length === 0) {
      return msg.sender.reply("No blocklisted channels found.", {
        color: Constants.errorColor,
      });
    }

    const channels = msg.dbGuild.youtubeChannels.blocklisted;

    let description = "";
    for (let i = 0; i < channels.length; i++) {
      description += channels[i] + "\n";
    }

    return msg.sender.send(description, { title: "Blocklisted Channels" });
  }
}

module.exports = new BlocklistedYouTubeChannels();
