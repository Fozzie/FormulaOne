const patron = require("patron");
const Discord = require("discord.js");

class TeamReaction extends patron.Command {
  constructor() {
    super({
      names: ["teamreaction", "tr"],
      group: "owner",
      description: "Sends a message that people react to gain / remove roles.",
    });
  }

  async run(msg, args) {
    msg.delete();
    const teamButtons = [
      [
        new Discord.MessageButton()
          .setCustomId("mercedes")
          .setEmoji("816627595243683860")
          .setStyle("SECONDARY"),
        new Discord.MessageButton()
          .setCustomId("redbull")
          .setEmoji("816627594353967155")
          .setStyle("SECONDARY"),
        new Discord.MessageButton()
          .setCustomId("ferrari")
          .setEmoji("816627595445534751")
          .setStyle("SECONDARY"),
        new Discord.MessageButton()
          .setCustomId("mclaren")
          .setEmoji("816627594542972928")
          .setStyle("SECONDARY"),
        new Discord.MessageButton()
          .setCustomId("alpine")
          .setEmoji("816627594492772362")
          .setStyle("SECONDARY"),
      ],
      [
        new Discord.MessageButton()
          .setCustomId("alphatauri")
          .setEmoji("816627594731716618")
          .setStyle("SECONDARY"),
        new Discord.MessageButton()
          .setCustomId("astonmartin")
          .setEmoji("816627595604000788")
          .setStyle("SECONDARY"),
        new Discord.MessageButton()
          .setCustomId("williams")
          .setEmoji("816627594261692447")
          .setStyle("SECONDARY"),
        new Discord.MessageButton()
          .setCustomId("alfaromeo")
          .setEmoji("816627594437984286")
          .setStyle("SECONDARY"),
        new Discord.MessageButton()
          .setCustomId("haas")
          .setEmoji("816627594538778644")
          .setStyle("SECONDARY"),
      ],
    ];
    const roleButtons = [
      [
        new Discord.MessageButton()
          .setCustomId("notification")
          .setEmoji("📣")
          .setStyle("SECONDARY"),
      ],
    ];
    msg.sender.send(
      "Click on the buttons below to assign your teams.",
      { title: "Choose your teams" },
      { components: teamButtons.map((b) => ({ type: 1, components: b })) }
    );
    msg.sender.send(
      "Click on the buttons below to assign other roles.\n\nClick on 📣 for notifications.",
      { title: "Choose your roles" },
      { components: roleButtons.map((b) => ({ type: 1, components: b })) }
    );
  }
}

module.exports = new TeamReaction();
