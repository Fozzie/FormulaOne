const patron = require("patron");
const Discord = require("discord.js");

class F1Reaction extends patron.Command {
  constructor() {
    super({
      names: ["f1reaction"],
      group: "owner",
      description: "Sends a message that people react to gain / remove roles.",
    });
  }

  async run(msg, args) {
    msg.delete();
    const buttons = [
      [
        new Discord.MessageButton()
          .setCustomId("f1serious")
          .setLabel("F1-Serious")
          .setStyle("SECONDARY"),
        new Discord.MessageButton()
          .setCustomId("f1technical")
          .setLabel("F1-Technical")
          .setStyle("SECONDARY"),
        new Discord.MessageButton()
          .setCustomId("assetto")
          .setLabel("Assetto")
          .setStyle("SECONDARY"),
      ],
    ];
    msg.sender.send(
      "By opting in for these channels, you understand that these channels may be highly moderated and that participants are held to a higher standard of civility" +
        " and respect, especially so in the <#186021984126107649> and <#433229818092322826> channels.\n\nDisciplinary action, such as removal from the channels, mutes, etc." +
        " may be imposed upon you at moderators' discretion if you break any of the channel rules. Read the pinned posts & channel descriptions in the channels, " +
        "as well as the server rules if in doubt.",
      { title: "Opt-in Channels" },
      { components: buttons.map((b) => ({ type: 1, components: b })) }
    );
  }
}

module.exports = new F1Reaction();
