const db = require("../../database/index.js");
const patron = require("patron");
const StringUtil = require("../../utility/StringUtil.js");

class ResetUser extends patron.Command {
  constructor() {
    super({
      names: ["resetuser"],
      group: "owner",
      description: "Reset any member's data.",
      arguments: [
        new patron.Argument({
          name: "member",
          key: "member",
          type: "member",
          defaultValue: patron.ArgumentDefault.Member,
          example: "Fozzie#0001",
          remainder: true,
        }),
      ],
    });
  }

  async run(msg, args) {
    await db.userRepo.deleteUser(args.member.id, msg.guild.id);

    return msg.sender.reply(
      "Successfully reset " +
        (args.member.id === msg.author.id
          ? "your"
          : StringUtil.boldify(args.member.user.tag) + "'s") +
        " data."
    );
  }
}

module.exports = new ResetUser();
