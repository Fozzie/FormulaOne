const patron = require("patron");
const db = require("../../database");
const Constants = require("../../utility/Constants.js");
const ModerationService = require("../../services/ModerationService.js");
const StringUtil = require("../../utility/StringUtil.js");

class Blocklist extends patron.Command {
  constructor() {
    super({
      names: ["bl", "blocklist"],
      group: "botowners",
      description: "Blocklist any user from using Formula One.",
      arguments: [
        new patron.Argument({
          name: "member",
          key: "member",
          type: "member",
          example: "User#4925",
          remainder: true,
        }),
      ],
    });
  }

  async run(msg, args) {
    const dbUser = await db.userRepo.getUser(args.member.id, msg.guild.id);
    if (dbUser.blocklisted) {
      await db.userRepo.upsertUser(args.member.id, msg.guild.id, {
        $set: { blocklisted: false },
      });
      await msg.sender.reply(
        "Successfully un-blocklisted " + StringUtil.boldify(args.member.user.tag)
      );
      return ModerationService.tryModLog(
        msg.dbGuild,
        msg.guild,
        "Bot Un-Blocklist",
        Constants.banColor,
        null,
        msg.author,
        args.member.user
      );
    } else {
      await db.userRepo.upsertUser(args.member.id, msg.guild.id, {
        $set: { blocklisted: true },
      });
      await msg.sender.reply(
        "Successfully blocklisted " + StringUtil.boldify(args.member.user.tag)
      );
      return ModerationService.tryModLog(
        msg.dbGuild,
        msg.guild,
        "Bot Blocklist",
        Constants.banColor,
        null,
        msg.author,
        args.member.user
      );
    }
  }
}

module.exports = new Blocklist();
