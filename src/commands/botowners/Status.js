const patron = require("patron");
const client = require("../../singletons/client.js");

class Status extends patron.Command {
  constructor() {
    super({
      names: ["status", "playing"],
      group: "botowners",
      description: "Set the bot status.",
      arguments: [
        new patron.Argument({
          name: "type",
          key: "type",
          type: "string",
          preconditions: ["statustype"],
          example: "PLAYING",
        }),
        new patron.Argument({
          name: "status",
          key: "status",
          type: "string",
          example: "$help",
          remainder: true,
        }),
      ],
    });
  }

  async run(msg, args) {
    await client.user.setActivity(args.status, { type: args.type });
    return msg.sender.reply(`Successfully set ${args.type} to ` + args.status);
  }
}

module.exports = new Status();
