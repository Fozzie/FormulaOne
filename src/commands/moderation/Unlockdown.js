const db = require("../../database/index");
const patron = require("patron");
const ModerationService = require("../../services/ModerationService.js");
const Constants = require("../../utility/Constants.js");
const Sender = require("../../utility/Sender.js");
const { Permissions } = require("discord.js");

class Unlockdown extends patron.Command {
  constructor() {
    super({
      names: ["unlockdown", "openup", "disablelockdown"],
      group: "moderation",
      description: "Remove the Lockdown for the guild",
    });
  }

  async run(msg, args) {
    if (!msg.dbGuild.lockdown) {
      return msg.sender.reply(`The guild is not in lockdown.`, {
        color: Constants.errorColor,
      });
    }

    const fanRole = msg.guild.roles.cache.get(Constants.roles.fan);
    const fanPermissions = fanRole.permissions.add(Permissions.FLAGS.SEND_MESSAGES);

    await fanRole.setPermissions(
      fanPermissions,
      "Lockdown disabled by " + msg.author.tag
    );

    await db.guildRepo.upsertGuild(msg.guild.id, { $set: { lockdown: false } });

    const announcementChannel = msg.guild.channels.cache.get(
      Constants.channels.announcements
    );
    await Sender.send(
      announcementChannel,
      "Lockdown has been disabled globally. Normal channel chat permissions now apply.",
      { timestamp: true }
    );

    await msg.sender.reply(`Successfully disabled lockdown.`);
    return ModerationService.tryLogAnything(
      msg.dbGuild,
      msg.guild,
      msg.author,
      "Toggled lockdown\n\n**Status:** Disabled.",
      null
    );
  }
}

module.exports = new Unlockdown();
