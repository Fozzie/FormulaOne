const patron = require("patron");
const Constants = require("../../../utility/Constants.js");
const Sender = require("../../../utility/Sender.js");

class Motorsport extends patron.Command {
  constructor() {
    super({
      names: ["ms", "motorsport", "motorsports"],
      group: "moderation",
      description:
        "Send the move to #motorsports message to a channel, <#432208507073331201> by default.",
      arguments: [
        new patron.Argument({
          name: "channel",
          key: "channel",
          type: "textchannel",
          example: "f1-discussion",
          defaultValue: Constants.channels.f1discussion,
        }),
      ],
    });
  }

  async run(msg, args) {
    const motorsportsChannel = await msg.guild.channels.resolve(
      Constants.channels.motorsports
    );
    if (msg.channel.id === args.channel.id) {
      msg.delete();
    } else {
      await msg.sender.reply("Successfully sent the message.");
    }

    if (args.channel === Constants.channels.f1discussion) {
      const channelToSend = await msg.guild.channels.resolve(
        Constants.channels.f1discussion
      );
      return Sender.send(
        channelToSend,
        "Please move to " + motorsportsChannel.toString() + "."
      );
    }

    return Sender.send(
      args.channel,
      "Please move to " + motorsportsChannel.toString() + "."
    );
  }
}

module.exports = new Motorsport();
