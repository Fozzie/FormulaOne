const patron = require("patron");
const Constants = require("../../../utility/Constants.js");
const Sender = require("../../../utility/Sender.js");

class F1General extends patron.Command {
  constructor() {
    super({
      names: ["fg", "f1g", "f1general"],
      group: "moderation",
      description:
        "Send the move to <#876046265111167016> message to a channel, <#432208507073331201> by default.",
      arguments: [
        new patron.Argument({
          name: "channel",
          key: "channel",
          type: "textchannel",
          example: "f1-discussion",
          defaultValue: Constants.channels.f1discussion,
        }),
      ],
    });
  }

  async run(msg, args) {
    const f1generalChannel = await msg.guild.channels.resolve(
      Constants.channels.f1general
    );
    if (msg.channel.id === args.channel.id) {
      msg.delete();
    } else {
      await msg.sender.reply("Successfully sent the message.");
    }

    if (args.channel === Constants.channels.f1discussion) {
      const channelToSend = await msg.guild.channels.resolve(
        Constants.channels.f1discussion
      );
      return Sender.send(
        channelToSend,
        "Please move to " + f1generalChannel.toString() + "."
      );
    }

    return Sender.send(
      args.channel,
      "Please move to " + f1generalChannel.toString() + "."
    );
  }
}

module.exports = new F1General();
