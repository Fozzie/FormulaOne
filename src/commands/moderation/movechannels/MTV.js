const patron = require("patron");
const Constants = require("../../../utility/Constants.js");
const Sender = require("../../../utility/Sender.js");

class MTV extends patron.Command {
  constructor() {
    super({
      names: ["mtv", "musictv"],
      group: "moderation",
      description:
        "Send the move to #music-tv-movies message to a channel, <#432208507073331201> by default.",
      arguments: [
        new patron.Argument({
          name: "channel",
          key: "channel",
          type: "textchannel",
          example: "f1-discussion",
          defaultValue: Constants.channels.f1discussion,
        }),
      ],
    });
  }

  async run(msg, args) {
    const mtvChannel = await msg.guild.channels.resolve(Constants.channels.mtv);
    if (msg.channel.id === args.channel.id) {
      msg.delete();
    } else {
      await msg.sender.reply("Successfully sent the message.");
    }

    if (args.channel === Constants.channels.f1discussion) {
      const channelToSend = await msg.guild.channels.resolve(
        Constants.channels.f1discussion
      );
      return Sender.send(
        channelToSend,
        "Please move to " + mtvChannel.toString() + "."
      );
    }

    return Sender.send(args.channel, "Please move to " + mtvChannel.toString() + ".");
  }
}

module.exports = new MTV();
