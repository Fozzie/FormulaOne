const patron = require("patron");
const Constants = require("../../../utility/Constants.js");
const Sender = require("../../../utility/Sender.js");

class Offtrack extends patron.Command {
  constructor() {
    super({
      names: ["ot", "offtrack"],
      group: "moderation",
      description:
        "Send the move to #offtrack message to a channel, <#432208507073331201> by default.",
      arguments: [
        new patron.Argument({
          name: "channel",
          key: "channel",
          type: "textchannel",
          example: "f1-discussion",
          defaultValue: Constants.channels.f1discussion,
        }),
      ],
    });
  }

  async run(msg, args) {
    const offtrackChannel = await msg.guild.channels.resolve(
      Constants.channels.offtrack
    );
    if (msg.channel.id === args.channel.id) {
      msg.delete();
    } else {
      await msg.sender.reply("Successfully sent the message.");
    }

    if (args.channel === Constants.channels.f1discussion) {
      const channelToSend = await msg.guild.channels.resolve(
        Constants.channels.f1discussion
      );
      return Sender.send(
        channelToSend,
        "Please move to " + offtrackChannel.toString() + "."
      );
    }

    return Sender.send(
      args.channel,
      "Please move to " + offtrackChannel.toString() + "."
    );
  }
}

module.exports = new Offtrack();
