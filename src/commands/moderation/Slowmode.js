const patron = require("patron");
const ModerationService = require("../../services/ModerationService.js");
const Constants = require("../../utility/Constants.js");

class Slowmode extends patron.Command {
  constructor() {
    super({
      names: ["slowmode", "sm"],
      group: "moderation",
      description: "Enable slowmode for a set period of time in seconds.",
      arguments: [
        new patron.Argument({
          name: "seconds",
          key: "seconds",
          type: "string",
          example: "5",
          remainder: true,
        }),
      ],
    });
  }

  async run(msg, args) {
    if (args.seconds === "off" || args.seconds === "disable" || args.seconds === "0") {
      await msg.channel.setRateLimitPerUser(
        0,
        "Slowmode Disabled by " + msg.author.tag
      );
      await msg.sender.reply(
        "Successfully disabled slowmode in " + msg.channel.toString() + "."
      );
      return ModerationService.tryLogAnything(
        msg.dbGuild,
        msg.guild,
        msg.author,
        "Disabled slowmode in " + msg.channel.toString() + ".",
        null
      );
    } else if (!isNaN(args.seconds)) {
      const seconds = parseInt(args.seconds);
      await msg.channel.setRateLimitPerUser(
        seconds,
        "Slowmode enabled by " + msg.author.tag
      );
      await msg.sender.reply(
        "Successfully enabled slowmode in " +
          msg.channel.toString() +
          " for " +
          args.seconds +
          " seconds."
      );
      return ModerationService.tryLogAnything(
        msg.dbGuild,
        msg.guild,
        msg.author,
        "Enabled slowmode in " +
          msg.channel.toString() +
          " for " +
          args.seconds +
          " seconds.",
        null
      );
    } else {
      return msg.sender.reply("You must specify a valid number.", {
        color: Constants.errorColor,
      });
    }
  }
}

module.exports = new Slowmode();
