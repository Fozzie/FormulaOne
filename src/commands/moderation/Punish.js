const patron = require("patron");
const Punishment = require("../../utility/Punishment.js");
const Try = require("../../utility/Try.js");

class Punish extends patron.Command {
  constructor() {
    super({
      names: ["punish"],
      group: "moderation",
      description:
        "Punish a member, (Use -s after the reason for auto deletion of the punishment message).",
      arguments: [
        new patron.Argument({
          name: "member",
          key: "member",
          type: "member",
          example: "John#5974",
          preconditions: ["nomoderator", "noself"],
        }),
        new patron.Argument({
          name: "reason",
          key: "reason",
          type: "string",
          example: "Spamming.",
          remainder: true,
        }),
      ],
    });
  }

  async run(msg, args) {
    await Try(msg.delete());
    return Punishment.runPunishCommand(msg, args);
  }
}

module.exports = new Punish();
