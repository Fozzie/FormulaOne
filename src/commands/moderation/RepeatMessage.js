const patron = require("patron");
const ModerationService = require("../../services/ModerationService.js");
const db = require("../../database/index.js");

class RepeatMessage extends patron.Command {
  constructor() {
    super({
      names: ["repeatmessage"],
      group: "moderation",
      description: "Toggle the repeating message for <#432208507073331201>",
    });
  }

  async run(msg, args) {
    if (msg.dbGuild.repeatMessageEnabled) {
      await ModerationService.tryLogAnything(
        msg.dbGuild,
        msg.guild,
        msg.author,
        "Disabled the repeating message <#432208507073331201>.",
        null
      );
      await db.guildRepo.upsertGuild(msg.guild.id, {
        $set: { repeatMessageEnabled: false },
      });
      return msg.sender.reply("Successfully disabled the repeating message.");
    }
    await ModerationService.tryLogAnything(
      msg.dbGuild,
      msg.guild,
      msg.author,
      "Enabled the repeating message in <#432208507073331201>.",
      null
    );
    await db.guildRepo.upsertGuild(msg.guild.id, {
      $set: { repeatMessageEnabled: true },
    });
    return msg.sender.reply("Successfully enabled the repeating message.");
  }
}

module.exports = new RepeatMessage();
