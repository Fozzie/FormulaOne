const db = require("../../database/index");
const patron = require("patron");
const ModerationService = require("../../services/ModerationService.js");
const Constants = require("../../utility/Constants.js");
const Sender = require("../../utility/Sender.js");
const { Permissions } = require("discord.js");

class Lockdown extends patron.Command {
  constructor() {
    super({
      names: ["lockdown", "enablelockdown"],
      group: "moderation",
      description: "Lockdown the guild",
    });
  }

  async run(msg, args) {
    if (msg.dbGuild.lockdown) {
      return msg.sender.reply(`The guild is already in lockdown.`, {
        color: Constants.errorColor,
      });
    }

    const fanRole = msg.guild.roles.cache.get(Constants.roles.fan);
    const fanPermissions = fanRole.permissions.remove(Permissions.FLAGS.SEND_MESSAGES);

    await fanRole.setPermissions(
      fanPermissions,
      "Lockdown enabled by " + msg.author.tag
    );

    await db.guildRepo.upsertGuild(msg.guild.id, { $set: { lockdown: true } });

    const announcementChannel = msg.guild.channels.cache.get(
      Constants.channels.announcements
    );
    await Sender.send(
      announcementChannel,
      "Lockdown has been enabled globally, only members with the F4+ role will be able to send messages.",
      { timestamp: true }
    );

    await msg.sender.reply(`Successfully enabled lockdown.`);
    return ModerationService.tryLogAnything(
      msg.dbGuild,
      msg.guild,
      msg.author,
      "Toggled lockdown\n\n**Status:** Enabled.",
      null
    );
  }
}

module.exports = new Lockdown();
