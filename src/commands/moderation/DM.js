const patron = require("patron");
const StringUtil = require("../../utility/StringUtil.js");

class DM extends patron.Command {
  constructor() {
    super({
      names: ["dm", "msg", "pm", "message"],
      group: "moderation",
      description: "DM a member a message.",
      arguments: [
        new patron.Argument({
          name: "member",
          key: "member",
          type: "member",
          example: "John#5974",
          preconditions: ["noself"],
        }),
        new patron.Argument({
          name: "message",
          key: "message",
          type: "string",
          example: "Please stop saying that, this won't count as a punishment.",
          defaultValue: "",
          remainder: true,
        }),
      ],
    });
  }

  async run(msg, args) {
    await msg.sender.dm(
      args.member.user,
      args.message,
      msg.channel,
      { title: "A message from a Moderator" },
      args.member.user
    );
    return msg.sender.reply(
      "Successfully DMed " + StringUtil.boldify(args.member.user.tag) + "."
    );
  }
}

module.exports = new DM();
