const patron = require("patron");
const db = require("../../database");
const Constants = require("../../utility/Constants.js");
const ModerationService = require("../../services/ModerationService.js");
const StringUtil = require("../../utility/StringUtil.js");
const NumberUtil = require("../../utility/NumberUtil.js");

class Tempban extends patron.Command {
  constructor() {
    super({
      names: ["tempban"],
      group: "moderation",
      description: "Tempbans any user for a specified time, default is minutes.",
      arguments: [
        new patron.Argument({
          name: "user",
          key: "user",
          type: "user",
          example: "Badboy#4925",
          preconditions: ["nomoderator"],
        }),
        new patron.Argument({
          name: "time",
          key: "time",
          type: "string",
          example: "10m",
        }),
        new patron.Argument({
          name: "reason",
          key: "reason",
          type: "string",
          example: "Breaking rules.",
          remainder: true,
        }),
      ],
    });
  }

  async run(msg, args) {
    const time = args.time.toLowerCase();
    const timeNum = time.replace(/\D/g, "");
    let timeMS;
    let timeUnit;
    if (time.includes("h")) {
      timeMS = NumberUtil.hoursToMs(timeNum);
      timeUnit = "hours";
    } else if (time.includes("m")) {
      timeMS = NumberUtil.minutesToMs(timeNum);
      timeUnit = "minutes";
    } else if (time.includes("d")) {
      timeMS = NumberUtil.daysToMs(timeNum);
      timeUnit = "days";
    } else if (!isNaN(time)) {
      timeMS = NumberUtil.minutesToMs(timeNum);
      timeUnit = "minutes";
    } else {
      return msg.sender.reply(
        "Invalid time format, formats: h (Hours), m (Minutes), d (Days).",
        { color: Constants.errorColor }
      );
    }

    if (await db.banRepo.anyBan(args.user.id, msg.guild.id)) {
      return msg.sender.reply("User is already temp-banned.", {
        color: Constants.errorColor,
      });
    }

    await msg.sender.dm(
      args.user,
      "A moderator has banned you for " +
        timeNum +
        " " +
        timeUnit +
        (StringUtil.isNullOrWhiteSpace(args.reason)
          ? "."
          : " for the reason: " + args.reason),
      msg.channel
    );
    await msg.guild.members.ban(args.user, {
      reason: `(${msg.author.tag}) ${timeNum} ${timeUnit} - ${args.reason}`,
    });
    await db.banRepo.insertBan(args.user.id, msg.guild.id, timeMS);
    await msg.sender.reply(
      "Successfully banned " +
        StringUtil.boldify(args.user.tag) +
        " for " +
        timeNum +
        " " +
        timeUnit +
        "."
    );
    await db.userRepo.upsertUser(args.user.id, msg.guild.id, {
      $inc: { bans: 1 },
    });
    await db.userRepo.upsertUser(
      args.user.id,
      msg.guild.id,
      new db.updates.Push("punishments", {
        date: Date.now(),
        escalation: timeNum + timeUnit + " Temp-Ban",
        reason: args.reason,
        mod: msg.author.tag,
        channelId: msg.channel.id,
      })
    );
    await db.userRepo.upsertUser(args.user.id, msg.guild.id, {
      $set: { joinedSinceBan: false },
    });
    return ModerationService.tryModLog(
      msg.dbGuild,
      msg.guild,
      "Temp-Ban",
      Constants.banColor,
      args.reason,
      msg.author,
      args.user,
      "Length",
      timeNum + " " + timeUnit
    );
  }
}

module.exports = new Tempban();
