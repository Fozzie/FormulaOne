const patron = require("patron");
const ModerationService = require("../../services/ModerationService.js");
const db = require("../../database/index.js");

class ToggleFilter extends patron.Command {
  constructor() {
    super({
      names: ["onewordfilter", "filter", "1wordfilter", "wordfilter"],
      group: "moderation",
      description: "Toggle the one word filter for this channel.",
    });
  }

  async run(msg, args) {
    const enabledText = "One word filter is enabled | ";
    const channelDescription = msg.channel.topic;
    if (msg.dbGuild.enabledChannels.includes(msg.channel.id)) {
      await db.guildRepo.upsertGuild(
        msg.guild.id,
        new db.updates.Pull("enabledChannels", msg.channel.id)
      );
      await ModerationService.tryLogAnything(
        msg.dbGuild,
        msg.guild,
        msg.author,
        "Toggled one word filter for " +
          msg.channel.toString() +
          "\n\n**Status**: Disabled.",
        null
      );
      await msg.sender.reply(
        "Successfully disabled one word filter for " + msg.channel.toString()
      );
      if (channelDescription != null) {
        return msg.channel.setTopic(channelDescription.replace(enabledText, ""));
      }
    } else {
      await db.guildRepo.upsertGuild(
        msg.guild.id,
        new db.updates.Push("enabledChannels", msg.channel.id)
      );
      await ModerationService.tryLogAnything(
        msg.dbGuild,
        msg.guild,
        msg.author,
        "Toggled one word filter for " +
        msg.channel.toString() +
        "\n\n**Status**: Enabled.",
        null
      );
      await msg.sender.reply(
        "Successfully enabled one word filter for " + msg.channel.toString()
      );
      if (channelDescription != null) {
        return msg.channel.setTopic(enabledText + channelDescription);
      }
    }
  }
}

module.exports = new ToggleFilter();
