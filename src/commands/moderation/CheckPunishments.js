const db = require("../../database/index");
const patron = require("patron");
const Punishment = require("../../utility/Punishment.js");
const StringUtil = require("../../utility/StringUtil.js");
const Constants = require("../../utility/Constants.js");
const Random = require("../../utility/Random.js");
const Discord = require("discord.js");

class CheckPunishments extends patron.Command {
  constructor() {
    super({
      names: [
        "checkpunishments",
        "checkpunishment",
        "checkpuns",
        "checkpun",
        "checkpunish",
      ],
      group: "moderation",
      description: "View a user's punishment history.",
      arguments: [
        new patron.Argument({
          name: "user",
          key: "user",
          type: "user",
          example: "John#5974",
          remainder: true,
        }),
      ],
    });
  }

  async run(msg, args) {
    const dbUser = await db.userRepo.getUser(args.user.id, msg.guild.id);
    if (dbUser.punishments.length === 0) {
      return msg.sender.send(
        StringUtil.boldify(args.user.tag) + " has a clean slate.",
        { color: Constants.greenColor }
      );
    }
    const embedColour = Random.arrayElement(Constants.defaultColors);
    const buttons = [
      [
        new Discord.MessageButton()
          .setCustomId(
            "ppage-0-" +
              args.user.id +
              "-" +
              Constants.defaultColors.indexOf(embedColour)
          )
          .setEmoji("⬅")
          .setStyle("SECONDARY")
          .setDisabled(true),
        new Discord.MessageButton()
          .setCustomId(
            "npage-0-" +
              args.user.id +
              "-" +
              Constants.defaultColors.indexOf(embedColour)
          )
          .setEmoji("➡")
          .setStyle("SECONDARY")
          .setDisabled(dbUser.punishments.length <= 5),
      ],
    ];

    const fields = await Punishment.getHistory(args.user, msg.guild, 0);

    const options = {
      title:
        args.user.tag +
        "'s Punishment History (1/" +
        (dbUser.punishments.length < 5
          ? 1
          : dbUser.punishments.length % 5 === 0
          ? dbUser.punishments.length / 5
          : Math.trunc(dbUser.punishments.length / 5) + 1) +
        ")",
      color: embedColour,
    };
    options.footer = {
      text:
        args.user.tag +
        " has " +
        dbUser.currentPunishment +
        " punishments in the last 30 days.",
    };

    return msg.sender.sendFields(fields, options, {
      components: buttons.map((b) => ({ type: 1, components: b })),
    });
  }
}

module.exports = new CheckPunishments();
