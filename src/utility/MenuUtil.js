const Constants = require("../utility/Constants.js");

class MenuUtil {
  getRulesMap() {
    const rules = Constants.rules;
    const options = [];
    rules.forEach((rule, i) =>
      options.push({
        label: `Rule ${i + 1}`,
        description: rule,
        value: `rule-${i}`,
      })
    ); // Ty ciel.
    return options;
  }

  getAmountMap(dbUser) {
    const amounts = [];
    for (let i = 1; i <= 5 - dbUser.currentPunishment; i++) {
      amounts.push(i.toString());
    }
    const options = [];
    amounts.forEach((amount) => options.push({ label: amount, value: amount }));
    return options;
  }
}

module.exports = new MenuUtil();
