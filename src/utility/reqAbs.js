const path = require("path");
const { RequireAll } = require("patron");

module.exports = async (name, dir) => RequireAll(path.join(name, dir));
