const credentials = require("../credentials.json");
const Logger = require("../utility/Logger.js");
const DiscoverAPIs = require("../utility/DiscoverAPIs.js");

class Perspective {
  async analyze(content) {
    const discovery = DiscoverAPIs.perspectiveDiscovery;
    const request = {
      comment: {
        text: content,
      },
      requestedAttributes: {
        SEVERE_TOXICITY: { scoreThreshold: 0.8 },
        IDENTITY_ATTACK: { scoreThreshold: 0.8 },
        THREAT: { scoreThreshold: 0.9 },
      },
    };
    const response = await discovery.comments
      .analyze({ key: credentials.perspectiveAPIKey, resource: request })
      .catch((err) => Logger.handlePerspectiveError(err));
    return response.data.attributeScores;
  }

  async suggest(content, attribute, score) {
    const discovery = DiscoverAPIs.perspectiveDiscovery;
    const request = {
      comment: {
        text: content,
      },
      attributeScores: {
        [attribute]: {
          summaryScore: {
            value: score,
          },
        },
      },
    };

    return discovery.comments
      .suggest({ key: credentials.perspectiveAPIKey, resource: request })
      .catch((err) => Logger.handlePerspectiveError(err));
  }
}

module.exports = new Perspective();
