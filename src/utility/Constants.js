const { Options, Sweepers } = require("discord.js");

class Constants {
  constructor() {
    this.defaultColors = [
      0xff269a, 0x00ff00, 0x00e828, 0x08f8ff, 0xf226ff, 0xff1c8e, 0x68ff22, 0xffbe11,
      0x2954ff, 0x9624ed, 0xa8ed00,
    ];
    this.errorColor = 0xff0000;
    this.greenColor = 0x00e828;
    this.banColor = 0xea0c00;
    this.kickColor = 0xe8511f;
    this.muteColor = 0xff720e;
    this.unbanColor = 0x13ff19;
    this.unmuteColor = 0x6ded5e;
    this.warnColor = 0xffb620;
    this.banishColor = 0xf47a42;

    this.game = "$help";
    this.type = "WATCHING";
    this.prefix = "$";

    this.intents = [
      "GUILDS",
      "GUILD_MEMBERS",
      "GUILD_MESSAGES",
      "GUILD_MESSAGE_REACTIONS",
      "GUILD_PRESENCES",
      "DIRECT_MESSAGES",
    ];

    this.cacheOptions = Options.cacheWithLimits({
      ...Options.defaultMakeCacheSettings,
      MessageManager: {
        maxSize: 200,
        sweepFilter: Sweepers.filterByLifetime({
          lifetime: 1800,
          getComparisonTimestamp: (e) => e.editedTimestamp ?? e.createdTimestamp,
        }),
        sweepInterval: 300,
      },
    });

    this.presence = {
      activities: [
        {
          name: this.game,
          type: this.type,
        },
      ],
    };

    this.modLog = "447397947261452288";
    this.botQueueLog = "920333278593024071";
    this.botQueueArchiveThread = "920333356250587156";
    this.stewardsQueueLog = "921485208681848842";
    this.botTesting = "424590803936083969";

    this.rules = [
      "Adhere to the Discord community guidelines and Terms of Service",
      "Circumventing moderation action is prohibited",
      "Illegal, harmful and NSFW/NSFL content is prohibited",
      "Do not send low-quality messages",
      "Be respectful and act in good faith",
      "Do not enforce these rules on behalf of a moderator",
      "No self-promotion",
      "Agree to disagree",
      "Usernames",
      "Use relevant channels and read the channel topic and pinned messages",
      "Keep all discussions in English",
      "Everyone is welcome",
    ];

    this.allowListedURLs = [
      "discord.com",
      "discord.gg",
      "discord.media",
      "discordapp.com",
      "discordapp.net",
      "discordstatus.com",
      "discord.gifts",
      "steampowered.com",
      "steamcommunity.com",
      "steam-chat.com",
    ];
    this.allowListedThirdParty = ["discord-avatar-maker.app"];

    this.channels = {
      announcements: "361137849736626177",
      motorsports: "247455736471224320",
      offtrack: "242392969213247500",
      f1general: "876046265111167016",
      f1discussion: "432208507073331201",
      f1beginners: "941601102049181737",
      f1serious: "186021984126107649",
      f1technical: "433229818092322826",
      sandbox: "242392574193565711",
      mtv: "338155101577281537",
    };

    this.regexes = {
      markdown: /([*~`_])+/g,
      markdownNoRepeat: /([*~`_])/g,
      escapedMarkdown: /\\([*~`_\\])+/g,
      escapedMarkdownNoRepeat: /\\([*~`_\\])/g,
      prefix: /^\$/,
      id: /^\d{17,20}$/,
      matchEmoji: /<:\w{2,32}:(\d{17,20})>/g,
      matchUserMention: /<@!?(\d{17,20})>/g,
      matchRoleMention: /<@&(\d{17,20})>/g,
      matchChannelMention: /<#(\d{17,20})>/g,
      onlyEmoji: /^<:\w{2,32}:(\d{17,20})>$/,
      onlyUnicode: /^[^\u0000-\u007F]+$/ /* eslint-disable-line no-control-regex */,
      onlyUserMention: /^<@!?(\d{17,20})>$/,
      onlyChannelMention: /^<#(\d{17,20})>$/,
      URLs: /https?:\/\/[^ \n]*/g,
      youtubeVideo: /youtu(?:.*\/v\/|.*v=|\.be\/)([A-Za-z0-9_\-]{11})/,
      youtubeChannel: /[A-Za-z0-9_\-]{24}/,
    };

    this.intervals = {
      joinRole: 30000,
      repeatMessage: 600000,
      lotto: 7200000,
      removeMemberRole: 60000,
      safeBrowsingCache: 5000,
      youtubeChannelCache: 3600000,
    };

    this.spam = {
      duration: 180,
      msgLimit: 3,
    };

    this.clear = {
      min: 1,
      max: 199,
    };

    this.buttonRoles = {
      mercedes: "186069675694751746",
      redbull: "186069703230226432",
      ferrari: "186069694451679232",
      mclaren: "186069741733937152",
      alpine: "186069769177399296",
      alphatauri: "186069710545092608",
      astonmartin: "186069729130184704",
      williams: "186069719567040512",
      alfaromeo: "186071442947964928",
      haas: "186069763372351488",
      f1serious: "594475219213877268",
      f1technical: "433231596095537152",
      assetto: "388802962782158861",
      notification: "941916641028096070",
    };

    this.roles = {
      fan: "328635502792278017",
      f1: "314910132733739009",
      f2: "314910011358707712",
      f4: "313677111695245312",
      helper: "941601929178533919",
    };

    this.banishedroles = {
      f1beginners: "941602889221165066",
      f1discussion: "821253279937462283",
      f1serious: "411950549064482823",
      f1technical: "433240749216104459",
    };

    this.banishedrolesmap = {
      [this.channels.f1beginners]: this.banishedroles.f1beginners,
      [this.channels.f1discussion]: this.banishedroles.f1discussion,
      [this.channels.f1serious]: this.banishedroles.f1serious,
      [this.channels.f1technical]: this.banishedroles.f1technical,
    };

    this.commandSimilarity = 0.66;
  }
}

module.exports = new Constants();
