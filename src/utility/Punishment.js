const db = require("../database/index.js");
const Constants = require("../utility/Constants.js");
const ModerationService = require("../services/ModerationService.js");
const StringUtil = require("../utility/StringUtil.js");

const punishmentLevels = [
  {
    type: "warn",
    display: "Warning",
    displayPastTense: "warned",
  },
  {
    type: "mute",
    length: 3600000,
    display: "1 hour mute",
    displayCurrent: "1 hour",
    displayPastTense: "muted",
  },
  {
    type: "mute",
    length: 86400000,
    display: "24 hour mute",
    displayCurrent: "24 hours",
    displayPastTense: "muted",
  },
  {
    type: "ban",
    length: 864000000,
    display: "10 day ban",
    displayCurrent: "10 days",
    displayPastTense: "banned",
  },
  {
    type: "ban",
    length: 2.592e9,
    display: "30 day ban",
    displayCurrent: "30 days",
    displayPastTense: "banned",
  },
];

class Punishment {
  async increasePunishment(memberId, guildId, amount) {
    await db.punRepo.insertPun(memberId, guildId, amount);
    return db.userRepo.upsertUser(memberId, guildId, {
      $inc: { currentPunishment: amount },
    });
  }

  async decreasePunishment(memberId, guildId) {
    const puns = await db.punRepo.findPun(memberId, guildId);
    const pun = puns[puns.length - 1];
    await db.punRepo.deleteById(pun._id);
    return db.userRepo.upsertUser(memberId, guildId, {
      $inc: { currentPunishment: -pun.amount },
    });
  }

  async runPunishCommand(msg, args, contextMenu = false, amount = 1) {
    const dbUser = await db.userRepo.getUser(args.member.id, msg.guild.id);
    const currentPun = await dbUser.currentPunishment;

    if (currentPun > punishmentLevels.length - 1) {
      return msg.sender.reply(
        `${StringUtil.boldify(args.member.user.tag)} has exceeded ${
          punishmentLevels.length
        } punishments in the last 30 days, escalate their punishment manually.`,
        { color: Constants.errorColor }
      );
    }

    const max =
      currentPun + amount > punishmentLevels.length
        ? punishmentLevels.length
        : currentPun + amount;
    const escalations = max - currentPun;

    const punishment = punishmentLevels[max - 1];
    await msg.sender.dm(
      args.member.user,
      `A moderator has ${punishment.displayPastTense} you${
        punishment.length != null ? ` for ${punishment.displayCurrent}` : ""
      }${
        StringUtil.isNullOrWhiteSpace(args.reason)
          ? ""
          : ` for the reason: ${args.reason}.`
      }${
        StringUtil.isNullOrWhiteSpace(args.messageContent)
          ? ""
          : ` The message you sent was: ${args.messageContent}`
      }.`,
      msg.channel
    );
    if (punishment.type === "mute") {
      await this.mute(
        args.member,
        msg.guild,
        msg.author,
        punishment.display,
        args.reason,
        msg.dbGuild,
        msg.sender,
        punishment.length
      );
    } else if (punishment.type === "ban") {
      await this.ban(
        args.member,
        msg.guild,
        msg.author,
        punishment.display,
        args.reason,
        punishment.length
      );
    }

    await db.userRepo.upsertUser(args.member.id, msg.guild.id, {
      $inc: { [`${punishment.type}s`]: 1 },
    });
    const reply = await msg.sender.send(
      `Successfully ${punishment.displayPastTense} ${StringUtil.boldify(
        args.member.user.tag
      )}${
        punishment.length != null
          ? ` for ${StringUtil.removeLastWord(punishment.display)}${
              punishment.length > 3600000 ? "s" : ""
            }`
          : ""
      }${
        contextMenu ? ` for the reason: ${args.reason}` : ""
      }.\n\nThey have ${currentPun} punishments in the last 30 days.`
    );
    await db.userRepo.upsertUser(
      args.member.id,
      msg.guild.id,
      new db.updates.Push("punishments", {
        date: Date.now(),
        escalation:
          `${currentPun + escalations} (${punishment.display})` +
          (escalations > 1 ? ` (${escalations} escalations)` : ""),
        reason: args.reason,
        mod: msg.author.tag,
        channelId: args.targetChannel ?? msg.channel.id,
        messageContent: args.messageContent,
      })
    );
    await this.increasePunishment(args.member.id, msg.guild.id, escalations);
    await ModerationService.tryModLog(
      msg.dbGuild,
      msg.guild,
      punishment.display + (escalations > 1 ? ` (${escalations} escalations)` : ""),
      Constants[`${punishment.type}Color`],
      args.reason,
      msg.author,
      args.member.user,
      "Message Content",
      args.messageContent,
      "Punishments in last 30 days",
      currentPun
    );
    if (
      msg.channel.id === Constants.botQueueLog ||
      msg.channel.id === Constants.stewardsQueueLog
    ) {
      return setTimeout(() => reply.delete(), 10000);
    }
  }

  async mute(member, guild, author, display, reason, dbGuild, sender, length) {
    const role = await guild.roles.fetch(dbGuild.roles.muted);
    if (member.roles.cache.has(dbGuild.roles.muted)) {
      await db.muteRepo.deleteMute(member.id, guild.id);
    }
    if (role == null) {
      return sender.reply(
        "The muted role is not set, please get an Administrator to set it using $setmutedrole <Role>",
        { color: Constants.errorColor }
      );
    }

    await member.disableCommunicationUntil(
      Date.now() + length,
      `(${author.tag} ${display} - ${reason})`
    );
    await member.roles.add(role);
    await db.muteRepo.insertMute(member.id, guild.id, length);
  }

  async ban(member, guild, author, display, reason, length) {
    if (await db.banRepo.anyBan(member.id, guild.id)) {
      await db.banRepo.deleteBan(member.id, guild.id);
    }
    await guild.members.ban(member.user, {
      reason: `(${author.tag}) ${display} - ${reason}`,
    });
    await db.banRepo.insertBan(member.id, guild.id, length);
  }

  async getHistory(user, guild, start) {
    const dbUser = await db.userRepo.getUser(user.id, guild.id);
    const allPuns = dbUser.punishments;
    dbUser.punishments.sort((a, b) => b.date - a.date);
    let fields = [];
    const end = start + 5;
    for (let i = start; i < (allPuns.length < end ? allPuns.length : end); i++) {
      const specificPun = allPuns[i];
      const specificDate = new Date(specificPun.date);
      const specificChannel = await guild.channels.resolve(specificPun.channelId);
      fields.push(specificDate.toGMTString());
      fields.push(
        "**Escalation:** " +
          specificPun.escalation +
          "\n**Moderator:** " +
          specificPun.mod +
          (specificPun.reason === "" ? "" : "\n**Reason:** " + specificPun.reason) +
          (specificPun.messageContent == null
            ? ""
            : "\n\n**Content:** " + specificPun.messageContent) +
          (specificChannel == null
            ? ""
            : "\n**Channel:** " + specificChannel.toString())
      );
    }

    return fields;
  }
}

module.exports = new Punishment();
