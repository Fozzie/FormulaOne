module.exports = async (promise, returnVal = false) => {
  try {
    const result = await promise;
    if (returnVal) {
      return result;
    } else {
      return true;
    }
  } catch (err) {
    return false;
  }
};
