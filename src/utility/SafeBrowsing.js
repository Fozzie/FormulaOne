const credentials = require("../credentials.json");
const Logger = require("../utility/Logger.js");
const db = require("../database/index.js");
const crypto = require("crypto");
const NumberUtil = require("../utility/NumberUtil.js");
const DiscoverAPIs = require("../utility/DiscoverAPIs.js");

class SafeBrowsing {
  async lookup(urls) {
    for (let i = 0; i < urls.length; i++) {
      const url = urls[i];
      const hash = crypto.createHash("sha256").update(url).digest("hex");
      const result = await db.safeBrowsingCacheRepo.findEntry(hash);
      if (result) {
        return {
          threatType: result.threatType,
          platformType: result.platformType,
        };
      }
    }
    const discovery = DiscoverAPIs.safeBrowsingDiscovery;
    const request = {
      client: {
        clientId: "r/formula1 Discord",
        clientVersion: "1.0",
      },
      threatInfo: {
        threatTypes: ["MALWARE", "SOCIAL_ENGINEERING", "UNWANTED_SOFTWARE"],
        platformTypes: ["ANY_PLATFORM"],
        threatEntryTypes: ["URL"],
        threatEntries: [],
      },
    };

    for (let i = 0; i < urls.length; i++) {
      request["threatInfo"]["threatEntries"].push({ url: urls[i] });
    }

    const response = await discovery.threatMatches
      .find({ key: credentials.safeBrowsingAPIKey, resource: request })
      .catch((err) => Logger.handleError(err));
    const matches = response.data.matches;

    if (matches == null) {
      return;
    }

    for (let i = 0; i < matches.length; i++) {
      const url = matches[i].threat.url;
      const hash = crypto.createHash("sha256").update(url).digest("hex");
      const cacheTime = NumberUtil.convertToMs(matches[i].cacheDuration);
      await db.safeBrowsingCacheRepo.insertEntry(
        hash,
        matches[i].threatType,
        matches[i].platformType,
        cacheTime
      );
    }

    const result = response.data.matches[0];
    return { threatType: result.threatType, platformType: result.platformType };
  }
}

module.exports = new SafeBrowsing();
