const USD = require("./USD.js");

class NumberUtil {
  isEven(num) {
    return num % 2 === 0;
  }

  msToTime(input) {
    return {
      milliseconds: parseInt((input % 1000) / 100),
      seconds: parseInt((input / 1000) % 60),
      minutes: parseInt((input / (1000 * 60)) % 60),
      hours: parseInt((input / (1000 * 60 * 60)) % 24),
      days: parseInt(input / (1000 * 60 * 60 * 24)),
    };
  }

  secondsToMs(input) {
    return input * 1000;
  }

  minutesToMs(input) {
    return input * 60000;
  }

  hoursToMs(input) {
    return input * 3600000;
  }

  daysToMs(input) {
    return input * 86400000;
  }

  pad(num, size) {
    let s = num.toString();

    while (s.length < size) {
      s = "0" + s;
    }

    return s;
  }

  realValue(input) {
    return input / 100;
  }

  format(input) {
    return USD(input / 100);
  }

  convertToMs(input) {
    const value = input.replace(/\D/, "");
    const functions = {
      s: this.secondsToMs,
      m: this.minutesToMs,
      h: this.hoursToMs,
      d: this.daysToMs,
    };

    return functions[input.slice(-1)](value);
  }

  ordinal(input) {
    const pluralRules = new Intl.PluralRules("en", { type: "ordinal" });
    const rules = { one: "st", two: "nd", few: "rd", other: "th" };
    return input + rules[pluralRules.select(input)];
  }
}

module.exports = new NumberUtil();
