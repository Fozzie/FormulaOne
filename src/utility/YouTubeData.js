const credentials = require("../credentials.json");
const Logger = require("../utility/Logger.js");
const DiscoverAPIs = require("../utility/DiscoverAPIs.js");
const db = require("../database/index.js");

class YouTubeData {
  async lookup(ids) {
    let idsFiltered = ids,
      results = [];
    for (let i = 0; i < ids.length; i++) {
      const result = await db.youtubeChannelCacheRepo.findEntry(ids[i]);
      if (result) {
        results.push({
          videoTitle: result.videoTitle,
          channelId: result.channelId,
        });
        idsFiltered = idsFiltered.filter((item) => item !== ids[i]);
      }
    }

    if (idsFiltered.length > 0) {
      const discovery = DiscoverAPIs.youtubeDiscovery;
      const request = {
        part: "snippet",
        id: idsFiltered,
        key: credentials.youtubeDataAPIKey,
      };

      const response = await discovery.videos
        .list(request)
        .catch((err) => Logger.handleError(err));
      const items = response.data.items;

      for (let i = 0; i < items.length; i++) {
        const videoTitle = items[i].snippet.title;
        const channelId = items[i].snippet.channelId;
        await db.youtubeChannelCacheRepo.insertEntry(
          idsFiltered[i],
          videoTitle,
          channelId
        );
        results.push({ videoTitle, channelId });
      }
    }

    return results;
  }
}

module.exports = new YouTubeData();
