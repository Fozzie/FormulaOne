const leven = require("leven");
const Constants = require("./Constants.js");

class Similarity {
  compare(a, b) {
    const distance = leven(a, b);
    const longest = a.length > b.length ? a : b;

    return 1 - distance / longest.length;
  }

  command(registry, input) {
    return [...registry.commands.keys()].find(
      (commandName) => this.compare(input, commandName) >= Constants.commandSimilarity
    );
  }
}

module.exports = new Similarity();
