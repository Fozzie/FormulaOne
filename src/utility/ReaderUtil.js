class ReaderUtil {
  handleMatches(msg, matches, userTag) {
    if (matches.size > 3) {
      return {
        match: false,
        error: "Multiple matches found, please be more specific.",
      };
    } else if (matches.size > 1) {
      const displayMatches = matches.map((match) =>
        userTag ? match.user.tag : match.toString()
      );
      return {
        match: false,
        error: `Multiple matches found: ${displayMatches.join(", ")}.`,
      };
    } else {
      return { match: matches.size === 1 };
    }
  }
}

module.exports = new ReaderUtil();
