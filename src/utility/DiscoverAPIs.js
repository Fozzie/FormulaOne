const { google } = require("googleapis");

class DiscoverAPIs {
  async discover() {
    this.perspectiveDiscovery = await google.discoverAPI(
      "https://commentanalyzer.googleapis.com/$discovery/rest?version=v1alpha1"
    );
    this.safeBrowsingDiscovery = await google.discoverAPI(
      "https://safebrowsing.googleapis.com/$discovery/rest?version=v4"
    );
    this.youtubeDiscovery = await google.discoverAPI(
      "https://youtube.googleapis.com/$discovery/rest?version=v3"
    );
  }
}

module.exports = new DiscoverAPIs();
