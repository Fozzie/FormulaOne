class Random {
  nextInt(min, max) {
    return Math.floor(Math.random() * (max - min)) + min;
  }

  nextFloat(min, max) {
    return this.nextInt(min * 100, max * 100 + 1) / 100;
  }

  roll() {
    return this.nextFloat(0, 100);
  }

  arrayElement(array) {
    return array[this.nextInt(0, array.length)];
  }
}

module.exports = new Random();
