const Embed = require("../structures/Embed.js");
const StringUtil = require("./StringUtil.js");
const NumberUtil = require("./NumberUtil.js");
const Constants = require("./Constants.js");
const Try = require("./Try.js");

class Sender {
  constructor(msg) {
    this.msg = msg;
  }

  async dm(user, description, channel, embedOptions = {}, messageOptions = {}) {
    const result = await Try(
      this.constructor.send(user, description, embedOptions, messageOptions)
    );
    if (!result && channel.id !== Constants.botQueueLog) {
      this.constructor.send(channel, "I do not have permission to DM that user.", {
        color: Constants.errorColor,
      });
    }
  }

  reply(description, embedOptions = {}, messageOptions = {}) {
    return this.constructor.reply(this.msg, description, embedOptions, messageOptions);
  }

  send(description, embedOptions = {}, messageOptions = {}) {
    return this.constructor.send(
      this.msg.channel,
      description,
      embedOptions,
      messageOptions
    );
  }

  sendFields(fieldsAndValues, embedOptions = {}, messageOptions = {}) {
    return this.constructor.sendFields(
      this.msg.channel,
      fieldsAndValues,
      embedOptions,
      messageOptions
    );
  }

  static reply(msg, description, embedOptions = {}, messageOptions = {}) {
    return this.send(
      msg.channel,
      StringUtil.boldify(msg.author.tag) + ", " + description,
      embedOptions,
      messageOptions
    );
  }

  static send(channel, description, embedOptions = {}, messageOptions = {}) {
    const descriptionType = typeof description;

    if (
      !StringUtil.isNullOrWhiteSpace(description) ||
      Object.keys(embedOptions).length > 0
    ) {
      if (descriptionType === "object") {
        embedOptions = description;
      } else if (descriptionType === "string") {
        embedOptions.description = description;
      } else {
        throw new Error("The description must be an object or a string.");
      }

      messageOptions.embeds = [new Embed(embedOptions)];
    }

    return channel.send(messageOptions);
  }

  static sendFields(channel, fieldsAndValues, embedOptions = {}, messageOptions = {}) {
    if (!NumberUtil.isEven(fieldsAndValues.length)) {
      throw new Error("The number of fields and values must be even.");
    }

    embedOptions.fields = [];

    for (let i = 0; i < fieldsAndValues.length - 1; i++) {
      if (NumberUtil.isEven(i)) {
        embedOptions.fields.push({
          name: fieldsAndValues[i],
          value: fieldsAndValues[i + 1].toString(),
        });
      }
    }

    return this.send(channel, "", embedOptions, messageOptions);
  }
}

module.exports = Sender;
