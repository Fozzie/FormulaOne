const Constants = require("./Constants.js");

class StringUtil {
  boldify(str) {
    return "**" + str.replace(Constants.regexes.markdown, "") + "**";
  }

  unlinkify(str) {
    return "`" + str.replace(Constants.regexes.markdown, "") + "`";
  }

  italicify(str) {
    return "_" + str.replace(Constants.regexes.markdown, "") + "_";
  }

  escape(input, removeNewLines = false) {
    if (typeof input === "string") {
      input = input
        .replace(Constants.regexes.escapedMarkdownNoRepeat, "$1")
        .replace(Constants.regexes.markdownNoRepeat, "\\$1");
      if (removeNewLines) {
        input = input.replaceAll("\n", "");
      }
      return input;
    } else {
      return input;
    }
  }

  isNullOrWhiteSpace(input) {
    return typeof input !== "string" || input.replace(/\s+/g, "").length === 0;
  }

  upperFirstChar(str) {
    return str.charAt(0).toUpperCase() + str.slice(1);
  }

  capitaliseEachWord(str) {
    return str.replace(/\w\S*/g, function (txt) {
      return txt.charAt(0).toUpperCase() + txt.substring(1).toLowerCase();
    });
  }

  removeLastWord(str) {
    const lastIndex = str.lastIndexOf(" ");
    return str.substring(0, lastIndex);
  }

  maxLength(str) {
    if (str.length > 100) {
      return str.substring(0, 100) + "...";
    }
    return str;
  }

  stripURL(url) {
    url = url.toLowerCase();
    if (url.endsWith("/")) {
      url = url.substring(0, url.length - 1);
    }
    return url;
  }

  removeClickableLinks(content) {
    const pattern = Constants.regexes.URLs;
    const urls = content.match(pattern);
    if (urls) {
      urls.forEach((url) => {
        content = content.replace(url, this.unlinkify(url));
      });
    }
    return content;
  }
}

module.exports = new StringUtil();
