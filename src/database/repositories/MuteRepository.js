const BaseRepository = require("./BaseRepository.js");
const UserQuery = require("../queries/UserQuery.js");
const Mute = require("../models/Mute.js");

class MuteRepository extends BaseRepository {
  async anyMute(userId, guildId) {
    return await this.any(new UserQuery(userId, guildId));
  }

  async insertMute(userId, guildId, muteLength) {
    return await this.insertOne(new Mute(userId, guildId, muteLength));
  }

  async findMute(userId, guildId) {
    return await this.findMany(new UserQuery(userId, guildId));
  }

  async deleteMute(userId, guildId) {
    return await this.deleteOne(new UserQuery(userId, guildId));
  }
}

module.exports = MuteRepository;
