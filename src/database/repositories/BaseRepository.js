const IdQuery = require("../queries/IdQuery.js");

class BaseRepository {
  constructor(collection) {
    this.collection = collection;
  }

  async any(filter) {
    const result = await this.count(filter);

    return result !== 0;
  }

  async count(filter) {
    return await this.collection.count(filter);
  }

  async findMany(filter = {}) {
    return await this.collection.find(filter).toArray();
  }

  async findOne(filter) {
    return await this.collection.findOne(filter);
  }

  async findById(id) {
    return await this.findOne(new IdQuery(id));
  }

  async insertMany(documents) {
    const result = await this.collection.insertMany(documents);

    return result.acknowledged;
  }

  async insertOne(document) {
    const result = await this.collection.insertOne(document);

    return result.acknowledged;
  }

  async replaceOne(filter, document) {
    return await this.collection.replaceOne(filter, document, { upsert: true });
  }

  async replaceById(id, document) {
    return await this.replaceOne(new IdQuery(id), document);
  }

  async findOneAndReplace(filter, document) {
    return await this.collection.findOneAndReplace(filter, document, {
      upsert: true,
      returnDocument: "after",
    });
  }

  async findByIdAndReplace(document) {
    return await this.findOneAndReplace(new IdQuery(document._id), document);
  }

  async updateMany(filter, update) {
    return await this.collection.updateMany(filter, update);
  }

  async updateOne(filter, update, upsert = false) {
    return await this.collection.updateOne(filter, update, { upsert: upsert });
  }

  async updateById(id, update, upsert = false) {
    return await this.updateOne(new IdQuery(id), update, upsert);
  }

  async findOneAndUpdate(filter, update, upsert = false) {
    return await this.collection.findOneAndUpdate(filter, update, {
      upsert: upsert,
      returnDocument: "after",
    });
  }

  async findByIdAndUpdate(id, update, upsert = false) {
    return await this.findOneAndUpdate(new IdQuery(id), update, upsert);
  }

  async deleteMany(filter) {
    return await this.collection.deleteMany(filter);
  }

  async deleteOne(filter) {
    return await this.collection.deleteOne(filter);
  }

  async deleteById(id) {
    return await this.deleteOne(new IdQuery(id));
  }

  async findOneAndDelete(filter) {
    return await this.collection.findOneAndDelete(filter);
  }

  findByIdAndDelete(id) {
    return this.findOneAndDelete(new IdQuery(id));
  }
}

module.exports = BaseRepository;
