const BaseRepository = require("./BaseRepository.js");
const UserQuery = require("../queries/UserQuery.js");
const User = require("../models/User.js");

class UserRepository extends BaseRepository {
  async anyUser(userId, guildId) {
    return await this.any(new UserQuery(userId, guildId));
  }

  async getUser(userId, guildId) {
    const query = new UserQuery(userId, guildId);
    const fetchedUser = await this.findOne(query, guildId);

    return (
      fetchedUser ?? (await this.findOneAndReplace(query, new User(userId, guildId)))
    );
  }

  async updateUser(userId, guildId, update) {
    return await this.updateOne(new UserQuery(userId, guildId), update);
  }

  async findUserAndUpdate(userId, guildId, update) {
    return await this.findOneAndUpdate(new UserQuery(userId, guildId), update);
  }

  async upsertUser(userId, guildId, update) {
    if (await this.anyUser(userId, guildId)) {
      return await this.updateUser(userId, guildId, update);
    }

    return await this.updateOne(new User(userId, guildId), update, true);
  }

  async findUserAndUpsert(userId, guildId, update) {
    if (await this.anyUser(userId, guildId)) {
      return await this.findUserAndUpdate(userId, guildId, update);
    }

    return await this.findOneAndUpdate(new User(userId, guildId), update, true);
  }

  async deleteUser(userId, guildId) {
    return await this.deleteOne(new UserQuery(userId, guildId));
  }

  async deleteUsers(guildId) {
    return await this.deleteMany({ guildId: guildId });
  }
}

module.exports = UserRepository;
