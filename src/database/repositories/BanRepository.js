const BaseRepository = require("./BaseRepository.js");
const UserQuery = require("../queries/UserQuery.js");
const Ban = require("../models/Ban.js");

class BanRepository extends BaseRepository {
  async anyBan(userId, guildId) {
    return await this.any(new UserQuery(userId, guildId));
  }

  async insertBan(userId, guildId, banLength) {
    return await this.insertOne(new Ban(userId, guildId, banLength));
  }

  async findBan(userId, guildId) {
    return await this.findMany(new UserQuery(userId, guildId));
  }

  async deleteBan(userId, guildId) {
    return await this.deleteOne(new UserQuery(userId, guildId));
  }
}

module.exports = BanRepository;
