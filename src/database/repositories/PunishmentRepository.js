const BaseRepository = require("./BaseRepository.js");
const UserQuery = require("../queries/UserQuery.js");
const Pun = require("../models/Pun.js");

class PunishmentRepository extends BaseRepository {
  async anyPun(userId, guildId) {
    return await this.any(new UserQuery(userId, guildId));
  }

  async insertPun(userId, guildId, amount) {
    return await this.insertOne(new Pun(userId, guildId, 2.592e9, amount)); // 2.592e+9 = 30 days.
  }

  async findPun(userId, guildId) {
    return await this.findMany(new UserQuery(userId, guildId));
  }

  async deletePun(userId, guildId) {
    return await this.deleteOne(new UserQuery(userId, guildId));
  }
}

module.exports = PunishmentRepository;
