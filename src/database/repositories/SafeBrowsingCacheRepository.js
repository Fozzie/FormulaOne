const BaseRepository = require("./BaseRepository.js");
const SafeBrowsingCacheQuery = require("../queries/SafeBrowsingCacheQuery.js");
const SafeBrowsingCache = require("../models/SafeBrowsingCache.js");

class SafeBrowsingCacheRepository extends BaseRepository {
  async findEntry(hash) {
    return await this.findOne(new SafeBrowsingCacheQuery(hash));
  }

  async insertEntry(hash, threatType, platformType, cacheLength) {
    return await this.insertOne(
      new SafeBrowsingCache(hash, threatType, platformType, cacheLength)
    );
  }

  async deleteEntry(hash) {
    return await this.deleteOne(new SafeBrowsingCacheQuery(hash));
  }
}

module.exports = SafeBrowsingCacheRepository;
