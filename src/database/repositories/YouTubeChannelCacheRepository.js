const BaseRepository = require("./BaseRepository.js");
const YouTubeChannelCacheQuery = require("../queries/YouTubeChannelCacheQuery.js");
const YouTubeChannelCache = require("../models/YouTubeChannelCache.js");

class YouTubeChannelCacheRepository extends BaseRepository {
  async findEntry(videoId) {
    return await this.findOne(new YouTubeChannelCacheQuery(videoId));
  }

  async insertEntry(videoId, videoTitle, channelId) {
    return await this.insertOne(
      new YouTubeChannelCache(videoId, videoTitle, channelId)
    );
  }

  async deleteEntry(videoId) {
    return await this.deleteOne(new YouTubeChannelCacheQuery(videoId));
  }
}

module.exports = YouTubeChannelCacheRepository;
