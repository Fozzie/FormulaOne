const BaseRepository = require("./BaseRepository.js");
const GuildQuery = require("../queries/GuildQuery.js");
const Guild = require("../models/Guild.js");

class GuildRepository extends BaseRepository {
  async anyGuild(guildId) {
    return await this.any(new GuildQuery(guildId));
  }

  async getGuild(guildId) {
    const query = new GuildQuery(guildId);
    const fetchedGuild = await this.findOne(query);

    return fetchedGuild ?? (await this.findOneAndReplace(query, new Guild(guildId)));
  }

  async updateGuild(guildId, update) {
    return await this.updateOne(new GuildQuery(guildId), update);
  }

  async findGuildAndUpdate(guildId, update) {
    return await this.findOneAndUpdate(new GuildQuery(guildId), update);
  }

  async upsertGuild(guildId, update) {
    if (await this.anyGuild(guildId)) {
      return await this.updateGuild(guildId, update);
    }

    return await this.updateOne(new Guild(guildId), update, true);
  }

  async findGuildAndUpsert(guildId, update) {
    if (await this.anyGuild(guildId)) {
      return await this.findGuildAndUpdate(guildId, update);
    }

    return await this.findOneAndUpdate(new Guild(guildId), update, true);
  }

  async deleteGuilds(guildId) {
    return await this.deleteMany({ guildId: guildId });
  }
}

module.exports = GuildRepository;
