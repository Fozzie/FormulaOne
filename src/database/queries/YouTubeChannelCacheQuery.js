class YouTubeChannelCacheQuery {
  constructor(videoId) {
    this.videoId = videoId;
  }
}

module.exports = YouTubeChannelCacheQuery;
