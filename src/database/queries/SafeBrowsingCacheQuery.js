class SafeBrowsingCacheQuery {
  constructor(hash) {
    this.hash = hash;
  }
}

module.exports = SafeBrowsingCacheQuery;
