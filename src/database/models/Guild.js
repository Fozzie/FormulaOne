class Guild {
  constructor(guildId) {
    this.guildId = guildId;
    this.caseNumber = 1;
    this.repeatMessageEnabled = false;
    this.lockdown = false;
    this.enabledChannels = [];
    this.roles = {
      mod: [],
      excluded: [],
      assignable: [],
      muted: undefined,
    };
    this.youtubeChannels = {
      blocklisted: [],
    };
  }
}

module.exports = Guild;
