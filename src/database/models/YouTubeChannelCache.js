class YouTubeChannelCache {
  constructor(videoId, videoTitle, channelId) {
    this.videoId = videoId;
    this.videoTitle = videoTitle;
    this.channelId = channelId;
    this.cachedAt = Date.now();
  }
}

module.exports = YouTubeChannelCache;
