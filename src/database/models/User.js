class User {
  constructor(userId, guildId) {
    this.userId = userId;
    this.guildId = guildId;
    this.currentPunishment = 0;
    this.warns = 0;
    this.mutes = 0;
    this.kicks = 0;
    this.bans = 0;
    this.punishments = [];
    this.blocklisted = false;
    this.leftRoles = [];
  }
}

module.exports = User;
