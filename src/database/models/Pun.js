class Pun {
  constructor(userId, guildId, punLength, amount) {
    this.userId = userId;
    this.guildId = guildId;
    this.punLength = punLength;
    this.punishedAt = Date.now();
    this.amount = amount;
  }
}

module.exports = Pun;
