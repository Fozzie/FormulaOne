class SafeBrowsingCache {
  constructor(hash, threatType, platformType, cacheLength) {
    this.hash = hash;
    this.threatType = threatType;
    this.platformType = platformType;
    this.cacheLength = cacheLength;
    this.cachedAt = Date.now();
  }
}

module.exports = SafeBrowsingCache;
