const MongoClient = require("mongodb").MongoClient;
const UserRepository = require("../repositories/UserRepository.js");
const GuildRepository = require("../repositories/GuildRepository.js");
const MuteRepository = require("../repositories/MuteRepository.js");
const PunishmentRepository = require("../repositories/PunishmentRepository.js");
const BanRepository = require("../repositories/BanRepository.js");
const SafeBrowsingCacheRepository = require("../repositories/SafeBrowsingCacheRepository.js");
const YouTubeChannelCacheRepository = require("../repositories/YouTubeChannelCacheRepository.js");

class Database {
  constructor() {
    this.queries = {
      Guild: require("../queries/GuildQuery.js"),
      Id: require("../queries/IdQuery.js"),
      User: require("../queries/UserQuery.js"),
      SafeBrowsingCache: require("../queries/SafeBrowsingCacheQuery.js"),
      YouTubeChannelCache: require("../queries/YouTubeChannelCacheQuery.js"),
    };

    this.updates = {
      Pull: require("../updates/PullUpdate.js"),
      Push: require("../updates/PushUpdate.js"),
      Pop: require("../updates/PopUpdate.js"),
    };

    this.models = {
      Guild: require("../models/Guild.js"),
      User: require("../models/User.js"),
      Mute: require("../models/Mute.js"),
      Pun: require("../models/Pun.js"),
      Ban: require("../models/Ban.js"),
      SafeBrowsingCache: require("../models/SafeBrowsingCache.js"),
      YouTubeChannelCache: require("../models/YouTubeChannelCache.js"),
    };
  }

  async connect(connectionURL, dbName) {
    const connection = await MongoClient.connect(connectionURL);
    const db = connection.db(dbName);

    this.guildRepo = new GuildRepository(await db.collection("guilds"));
    this.userRepo = new UserRepository(await db.collection("users"));
    this.muteRepo = new MuteRepository(await db.collection("mutes"));
    this.punRepo = new PunishmentRepository(await db.collection("punishments"));
    this.banRepo = new BanRepository(await db.collection("bans"));
    this.safeBrowsingCacheRepo = new SafeBrowsingCacheRepository(
      await db.collection("safeBrowsingCache")
    );
    this.youtubeChannelCacheRepo = new YouTubeChannelCacheRepository(
      await db.collection("youtubeChannelCache")
    );

    await db.collection("guilds").createIndex("guildId", { unique: true });
    await db
      .collection("users")
      .createIndex({ userId: 1, guildId: 1 }, { unique: true });
    await db
      .collection("mutes")
      .createIndex({ userId: 1, guildId: 1 }, { unique: true });
    await db
      .collection("punishments")
      .createIndex({ userId: 1, guildId: 1 }, { unique: false });
    await db
      .collection("bans")
      .createIndex({ userId: 1, guildId: 1 }, { unique: true });
    await db.collection("safeBrowsingCache").createIndex("hash", { unique: true });
    await db.collection("youtubeChannelCache").createIndex("videoId", { unique: true });
  }
}

module.exports = Database;
