const discord = require("discord.js");
const Random = require("../utility/Random.js");
const Constants = require("../utility/Constants.js");

class Embed extends discord.MessageEmbed {
  constructor(data) {
    if (data.fields != null) {
      const inline = data.inline != null ? data.inline : false;

      for (let i = 0; i < data.fields.length; i++) {
        data.fields[i].inline = inline;
      }
    }

    data.timestamp = data.timestamp === true ? new Date() : undefined;
    data.footer =
      data.footer != null
        ? { text: data.footer.text, icon_url: data.footer.icon }
        : undefined;
    data.color =
      data.color != null ? data.color : Random.arrayElement(Constants.defaultColors);

    super(data);
  }
}

module.exports = Embed;
