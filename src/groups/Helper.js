const patron = require("patron");

class Helper extends patron.Group {
  constructor() {
    super({
      name: "helper",
      preconditions: ["helper"],
    });
  }
}

module.exports = new Helper();
