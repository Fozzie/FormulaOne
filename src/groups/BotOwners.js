const patron = require("patron");

class BotOwners extends patron.Group {
  constructor() {
    super({
      name: "botowners",
      description: "These commands may only be used by the owners of F1.",
      preconditions: ["botowner"],
    });
  }
}

module.exports = new BotOwners();
