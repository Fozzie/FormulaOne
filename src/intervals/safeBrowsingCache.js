const db = require("../database");
const Constants = require("../utility/Constants.js");
const Logger = require("../utility/Logger.js");

setInterval(() => {
  (async function () {
    const entries = await db.safeBrowsingCacheRepo.findMany();

    for (let i = 0; i < entries.length; i++) {
      if (entries[i].cachedAt + entries[i].cacheLength > Date.now()) {
        continue;
      }

      await db.safeBrowsingCacheRepo.deleteById(entries[i]._id);
    }
  })().catch((err) => Logger.handleError(err));
}, Constants.intervals.safeBrowsingCache);
