const db = require("../database");
const Constants = require("../utility/Constants.js");
const Logger = require("../utility/Logger.js");

setInterval(() => {
  (async function () {
    const entries = await db.youtubeChannelCacheRepo.findMany();

    for (let i = 0; i < entries.length; i++) {
      if (entries[i].cachedAt + 1.21e9 > Date.now()) {
        // 1.21e+9 = 14 days.
        continue;
      }

      await db.youtubeChannelCacheRepo.deleteById(entries[i]._id);
    }
  })().catch((err) => Logger.handleError(err));
}, Constants.intervals.youtubeChannelCache);
